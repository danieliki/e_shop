﻿using System.Collections.Generic;
using System.Web.Mvc;

using E_Shop.CORE.Domain.Classes;

namespace E_Shop.Web.Models.Shop
{
    public class OrderViewModel
    {
        public ApplicationUser User { get; set; }

        public Address Address { get; set; }

        public Order Order { get; set; }

        public List<SelectListItem> States { get; set; }
    }
}