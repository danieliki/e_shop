﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;
using E_Shop.Web.JQueryDataTable;
using E_Shop.Web.Models.Shop;

namespace E_Shop.Web.Controllers
{
    public class ShopController : Controller
    {
        private IProductManager productManager;
        private IFamilyManager familyManager;
        private IVarietyManager varietyManager;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="productManager"></param>
        /// <param name="familyManager"></param>
        /// <param name="varietyManager"></param>
        public ShopController(IProductManager productManager, IFamilyManager familyManager, IVarietyManager varietyManager)
        {
            this.productManager = productManager;
            this.familyManager = familyManager;
            this.varietyManager = varietyManager;
        }
   
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var model = varietyManager.GetAll()
                    .Select(v => new ListViewModel()
                    {
                        Id = v.Id,
                        Name = v.Product.Brand + " - " + v.Product.Model,
                        Brand = v.Product.Brand,
                        Price = v.Product.Price,
                      
                    }).ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult FamilyParentMenuPartial()
        {
            var listSon = new List<List<List<Family>>>();
            var listSon2 = new List<List<Family>>();
            var sonFamilies = new List<Family>();
            var parentFamilies = familyManager.GetAllByParentId(1).ToList();
            var pfam = familyManager.GetAllByParentId(1).Select(p => p.Id).ToList();


            foreach (var f in pfam)
            {
                sonFamilies = familyManager.GetAllByParentId(f).ToList();
                listSon2 = sonFamilies.GroupBy(c => c.ParentId).Select(group => group.ToList()).ToList();
                listSon.Add(listSon2);               
            }

            FamilyMenuViewModel model = new FamilyMenuViewModel()
                                            {
                                                ParentFamily = parentFamilies,
                                                SonFamily = listSon
                                            };

            return PartialView("FamilyParentMenuPartial", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="familyId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ListByFamily()  
        {
            var model = productManager.GetByFamily(1)
                    .Select(p => new ListViewModel()
                    {
                        Id = p.Id,
                        Name = p.Brand + " - " + p.Model,
                        Price = p.Price,
                    }).ToList();

            return PartialView("_ListPartial", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="brand"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ListByBrand(string brand)
        {
            var model = productManager.GetByBrand(brand)
                    .Select(p => new ListViewModel()
                    {
                        Id = p.Id,
                        Name = p.Brand + " - " + p.Model,
                        Price = p.Price,
                    }).ToList();

            return PartialView("_ListPartial", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="price1"></param>
        /// <param name="price2"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ListByPrice(decimal price1, decimal price2)
        {
            var model = productManager.GetByPrice(price1, price2)
                    .Select(p => new ListViewModel()
                    {
                        Id = p.Id,
                        Name = p.Brand + " - " + p.Model,
                        Price = p.Price,
                    }).ToList();

            return PartialView("_ListPartial", model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Details(int id)
        {
            var variety = varietyManager.GetById(id);
            var imageFiles = new List<string>();

            var referenceUri = new Uri(@Server.MapPath("~\\Images\\Product\\"));

            foreach (var file in Directory.GetFiles(@Server.MapPath("~\\Images\\Product\\"), id.ToString() + "_*"))
            {
                var fileUri = new Uri(file);
                imageFiles.Add("/Images/Product/" + referenceUri.MakeRelativeUri(fileUri).ToString());
            }

            DetailsViewModel model = new DetailsViewModel()
                                         {
                                             Id = id,
                                             Name = variety.Product.FullName,
                                             Price = variety.Product.Price.ToString("0,##"),
                                             Weight = variety.Weight.ToString("0,##"),
                                             Variety = variety,
                                             Stock = variety.Stock,
                                             ImagesFiles = imageFiles
                                         };

            
         
            return View(model);
        }

        //public ActionResult GetProductVarieties()
        //{
            
            
        //    return PartialView("GetProductVarieties", Lalala());
        //}

        //public IList<DetailsViewModel> Lalala()
        //{
        //    var variety = varietyManager.GetByProductId(id);
        //    var imageFiles = new List<string>();

        //    var referenceUri = new Uri(@Server.MapPath("~\\Images\\Product\\"));

        //    foreach (var file in Directory.GetFiles(@Server.MapPath("~\\Images\\Product\\"), variety.Id.ToString() + "_*"))
        //    {
        //        var fileUri = new Uri(file);
        //        imageFiles.Add("/Images/Product/" + referenceUri.MakeRelativeUri(fileUri).ToString());
        //    }

        //    var model = varietyManager.GetAllByProductId(id).Select(v => new DetailsViewModel()
        //    {
        //        Id = v.Id,
        //        ProductId = v.ProductId,
        //        Image = imageFiles.FirstOrDefault()
        //    }).ToList();

        //    return (model);
        //} 


        //public ActionResult FiltrarPorFamilia()
        //{
        //    return View()
        //}
       
    }
}
