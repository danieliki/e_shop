﻿using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;

namespace E_Shop.Application
{
    /// <summary>
    /// Manager de los IVAS
    /// </summary>
    public class IVAManager : Manager<IVA>, IIVAManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="db"></param>
        public IVAManager(IApplicationDbContext db)
            : base(db)
        {
        }
    }
}