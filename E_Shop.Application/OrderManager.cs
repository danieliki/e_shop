﻿using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;

namespace E_Shop.Application
{
    /// <summary>
    /// Manager de pedidos
    /// </summary>
    public class OrderManager : Manager<Order>, IOrderManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="db"></param>
        public OrderManager(IApplicationDbContext db)
            : base(db)
        {
        }
    }
}