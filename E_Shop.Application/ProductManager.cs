﻿using System.Linq;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;

namespace E_Shop.Application
{
    /// <summary>
    /// Manager de productos
    /// </summary>
    public class ProductManager : Manager<Product>, IProductManager
    {
        /// <summary>
        /// Constructor de la clase ProductManager
        /// </summary>
        /// <param name="context"></param>
        public ProductManager(IApplicationDbContext context) : base(context)
        {    
        }

        /// <summary>
        /// Método que devuelve un producto por su marca
        /// </summary>
        /// <param name="brand"></param>
        /// <returns></returns>
        public IQueryable<Product> GetAllByBrand(string brand)
        {
            return db.Products.Where(p => p.Brand == brand);
        }

        /// <summary>
        /// Método que devuelve los productoss comprendidos entre un rango de precios
        /// </summary>
        /// <param name="price1"></param>
        /// <param name="price2"></param>
        /// <returns></returns>
        public IQueryable<Product> GetAllByPrice(decimal price1, decimal price2)
        {
            return db.Products.Where(p => p.Price >= price1 && p.Price <= price2);
        }

        /// <summary>
        /// Método que devuelve un producto por su id de familia
        /// </summary>
        /// <param name="familyId"></param>
        /// <returns></returns>
        public IQueryable<Product> GetAllByFamily(int familyId)
        {
            return db.Products.Where(p => p.FamilyId == familyId);
        }
    }
}