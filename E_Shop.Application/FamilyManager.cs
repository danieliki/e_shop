﻿using System.Linq;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;
using E_Shop.DAL;

namespace E_Shop.Application
{
    /// <summary>
    /// Manager de las familias de los productos
    /// </summary>
    public class FamilyManager : Manager<Family>, IFamilyManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="db"></param>
        public FamilyManager(IApplicationDbContext db)
            : base(db)
        {
        }

        /// <summary>
        /// Método que devuelve la primera familia de un padre en concreto
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public Family GetByParentId(int parentId)
        {
            return db.Families.FirstOrDefault(f => f.ParentId == parentId);
        }

        /// <summary>
        /// Método que devuelve todas las familias con un padre en concreto
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public IQueryable<Family> GetAllByParentId(int parentId)
        {
            return db.Families.Where(f => f.ParentId == parentId);
        } 
    }
}