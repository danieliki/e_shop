﻿using System.Linq;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;

namespace E_Shop.Application
{
    /// <summary>
    /// Manager de los comentarios de una variedad
    /// </summary>
    public class CommentManager : Manager<Comment>, ICommentManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="db"></param>
        public CommentManager(IApplicationDbContext _db)
            : base(_db)
        {
        }

        /// <summary>
        /// Método que devuelve todos los comentarios de una variedad
        /// </summary>
        /// <param name="varietyId"></param>
        /// <returns></returns>
        public IQueryable<Comment> GetAllByVarietyId(int varietyId)
        {
            return db.Comments.Where(c => c.VarietyId == varietyId);
        } 
    }
}
