﻿using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;

namespace E_Shop.Application.MethodPayments
{
    /// <summary>
    /// Manager con la lógica del método de pago paypal
    /// </summary>
    public class Transferencia : IMethodPayment
    {
        /// <summary>
        /// Propiedad que recoge el nombre del método de pago
        /// </summary>
        public string name
        {
            get
            {
                return "Transferencia";
            }
        }

        /// <summary>
        /// Método que genera la operación de pago
        /// </summary>
        /// <param name="order"></param>
        public void Payment(Order order)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Método que devuelve el resultado favorable de la operación
        /// </summary>
        /// <param name="orderId"></param>
        public void RequestOK(int orderId)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Método que devuelve el resultado fallido de la operación
        /// </summary>
        /// <param name="orderId"></param>
        public void RequestKO(int orderId)
        {
            throw new System.NotImplementedException();
        }
    }
}