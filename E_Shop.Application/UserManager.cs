﻿using System.Linq;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;

namespace E_Shop.Application
{
    /// <summary>
    /// Manager de los usuarios
    /// </summary>
    public class UserManager : Manager<ApplicationUser>, IUserManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="_db"></param>
        public UserManager(IApplicationDbContext _db)
            : base(_db)
        {
        }

        /// <summary>
        /// Método que devuelve un usuario por su nombre de usuario
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public ApplicationUser GetByUserName(string name)
        {
            return db.Users.SingleOrDefault(u => u.UserName == name);
        }
    }
}