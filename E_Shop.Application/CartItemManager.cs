﻿using System.Data.Entity;
using System.Linq;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;

namespace E_Shop.Application
{
    /// <summary>
    /// Manager de los items del carrito
    /// </summary>
    public class CartItemManager : Manager<CartItem>, ICartItemManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="db"></param>
        public CartItemManager(IApplicationDbContext db)
            : base(db)
        {
        }

        /// <summary>
        /// Método que devuelve los items de un carrito de una sesión en concreto
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="session"></param>
        /// <returns></returns>
        public IQueryable<CartItem> GetCart(string userId, string session)
        {
            if (!string.IsNullOrWhiteSpace(userId))
            {
                return db.CartItems.Include(c => c.Variety).Where(c => c.User.Id == userId);
            }
            return db.CartItems.Include(c => c.Variety).Where(c => c.User.Id == userId && c.Session == session);
        }

        /// <summary>
        /// Método que guarda en la sesión los items del carrito
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="session"></param>
        public void Login(string userId, string session)
        {
            var cartItems = GetCart(null, session);
            foreach (var cartItem in cartItems)
            {
                cartItem.UserId = userId;
            }
            db.SaveChanges();
        }
    }
}