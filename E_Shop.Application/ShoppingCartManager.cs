﻿using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;

namespace E_Shop.Application
{
    /// <summary>
    /// Manager de carritos de la compra
    /// </summary>
    public class ShoppingCartManager : Manager<ShoppingCart>, IShoppingCartManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="db"></param>
        public ShoppingCartManager(IApplicationDbContext db)
            : base(db)
        {
        }
    }
}