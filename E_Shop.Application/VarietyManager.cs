﻿using System.Linq;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;

namespace E_Shop.Application
{
    public class VarietyManager : Manager<Variety>, IVarietyManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="_db"></param>
        public VarietyManager(IApplicationDbContext _db)
            : base(_db)
        {
        }

        /// <summary>
        /// Método que devuelve todas variedades de un producto por su id de producto y que tenga unidades
        /// </summary>
        /// <param name="productid"></param>
        /// <returns></returns>
        public IQueryable<Variety> GetAllByProductId(int? productid)
        {
            return db.Varieties.Where(v => v.ProductId == productid);
        }

        /// <summary>
        /// Método que devuelve todas la variedades que tenga unidades
        /// </summary>
        /// <returns></returns>
        public IQueryable<Variety> GetAllOnSale()
        {
            return db.Varieties.Where(v => v.OnSale && v.Stock > 0);
        }

        /// <summary>
        /// Método que devuelve todas las variedades de un color
        /// </summary>
        /// <param name="colour"></param>
        /// <returns></returns>
        public IQueryable <Variety> GetAllByColour(string colour)
        {
            return db.Varieties.Where(v => v.Colour == colour);
        }


        /// <summary>
        /// Método que devuelve una variedad por su Permalink
        /// </summary>
        /// <param name="permalink"></param>
        /// <returns></returns>
        public Variety GetByPermalink(string permalink)
        {
            return db.Varieties.SingleOrDefault(v => v.Permalink.ToLower() == permalink.ToLower());
        }

        /// <summary>
        /// Método que devuelve una variedad por su id de producto
        /// </summary>
        /// <param name="productid"></param>
        /// <returns></returns>
        public Variety GetByProductId(int productid)
        {
            return db.Varieties.FirstOrDefault(v => v.ProductId == productid && v.Stock > 0);
        }

        /// <summary>
        /// Método que devuelve todas las variedades por la marca del producto al que pertenecen
        /// </summary>
        /// <param name="brand"></param>
        /// <returns></returns>
        public IQueryable<Variety> GetAllByProductBrand(string brand)
        {
            return db.Varieties.Where(v => v.Product.Brand == brand && v.Stock > 0);
        }

        /// <summary>
        /// Método que devuelve todas las variedades por la familia del producto al que pertenecen
        /// </summary>
        /// <param name="familyId"></param>
        /// <returns></returns>
        public IQueryable<Variety> GetAllByProductFamily(int familyId)
        {
            return db.Varieties.Where(v => v.Product.FamilyId == familyId && v.Stock > 0);
        }

        /// <summary>
        /// Método que devuelve todas las variedades comprendidas en un rango de precios
        /// </summary>
        /// <param name="price1"></param>
        /// <param name="price2"></param>
        /// <returns></returns>
        public IQueryable<Variety> GetAllByProductPriceRange(decimal price1, decimal price2)
        {
            return db.Varieties.Where(v => v.Product.Price >= price1 && v.Product.Price <= price2 && v.Stock > 0);
        }
    }
}