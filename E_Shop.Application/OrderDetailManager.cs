﻿using System.Linq;

using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;

namespace E_Shop.Application
{
    /// <summary>
    /// Manager de las líneas de un pedido
    /// </summary>
    public class OrderDetailManager : Manager<OrderDetail>, IOrderDetailManager
    {
        /// <summary>
        /// Constructor del manager
        /// </summary>
        /// <param name="_db"></param>
        public OrderDetailManager(IApplicationDbContext _db)
            : base(_db)
        {
        }

        public IQueryable<OrderDetail> GetAllByOrderId(int orderId)
        {
            return db.OrderDetails.Where(o => o.OrderId == orderId);
        }
    }
}