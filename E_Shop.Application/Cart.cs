﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;
using E_Shop.DAL;

namespace E_Shop.Application
{
    /// <summary>
    /// Clase que representa un carrito de la compra en función de su estado de sesión
    /// </summary>
    public class Cart
    {
        public string ShoppingCartId { get; set; }
        public const string CartSessionKey = "CartId";
        public ApplicationDbContext db = new ApplicationDbContext();

        /// <summary>
        /// Método estático para instanciarlo desde otros puntos de la aplicación
        /// </summary>
        /// <param name="context"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static Cart GetCart(HttpContextBase context)
        {
            var cart = new Cart();
            cart.ShoppingCartId = cart.GetCartId(context);
            return cart;
        }

        /// <summary>
        /// Método estático para instanciarlo desde otros puntos de la aplicación
        /// </summary>
        /// <param name="controller"></param>
        /// <returns></returns>
        public static Cart GetCart(Controller controller)
        {
            return GetCart(controller.HttpContext);
        }


        /// <summary>
        /// Método que devuelve todos los productos de un carrito de la compra
        /// </summary>
        /// <returns></returns>
        public List<ShoppingCart> GetCartItems()
        {
            return db.ShoppingCarts.Where(cart => cart.CartId == ShoppingCartId).ToList();
        }

        /// <summary>
        /// Método que devuelve el número de variedades que hay en cada item del carrito
        /// </summary>
        /// <returns></returns>
        public int GetCount()
        {
            int? count =
                (from cartItems in db.ShoppingCarts
                 where cartItems.CartId == ShoppingCartId
                 select (int?)cartItems.Count).Sum();

            return count ?? 0;
        }

        /// <summary>
        /// Método que devuelve el importe total de cada item del carrito
        /// </summary>
        /// <returns></returns>
        public decimal GetTotal()
        {
            decimal? total = (from cartItems in db.ShoppingCarts
                              where cartItems.CartId == ShoppingCartId
                              select (int?)cartItems.Count * cartItems.Variety.Product.Price).Sum();

            return total ?? decimal.Zero;
        }

        /// <summary>
        /// Método que devuelve el IVA de cada item del carrito
        /// </summary>
        /// <returns></returns>
        public decimal GetIVA()
        {
            decimal iva = db.ShoppingCarts.Where(s => s.CartId == ShoppingCartId).Select(s => s.Variety.Product.IVA.Value).FirstOrDefault();
            return decimal.Round(iva ,2, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// Método que devuelve el id de sesión del carrito
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public string GetCartId(HttpContextBase context)
        {
            if (context.Session[CartSessionKey] == null)
            {
                if (!string.IsNullOrWhiteSpace(context.User.Identity.Name))
                {
                    context.Session[CartSessionKey] = context.User.Identity.Name;
                }
                else
                {
                    Guid tempCartId = Guid.NewGuid();

                    context.Session[CartSessionKey] = tempCartId.ToString();
                }
            }
            return context.Session[CartSessionKey].ToString();
        }

        /// <summary>
        /// Método que migra el carrito de una sesión a otra
        /// </summary>
        /// <param name="userName"></param>
        public void MigrateCart(string userName)
        {
            var shoppingCart = db.ShoppingCarts.Where(c => c.CartId == ShoppingCartId);

            foreach (ShoppingCart item in shoppingCart)
            {
                item.CartId = userName;
            }
            db.SaveChanges();
        }

        /// <summary>
        /// Método de creación del pedido
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public int CreateOrder(Order order)
        {
            decimal orderTotal = 0;
            var cartItems = GetCartItems();
            try
            {
                
                    foreach (var item in cartItems)
                    {
                        var orderDetail = new OrderDetail
                        {
                            VarietyId = item.VarietyId,
                            Variety = item.Variety,
                            OrderId = order.Id,
                            Quantity = item.Count
                        };

                        orderTotal += (item.Count * item.Variety.Product.Price);

                        db.OrderDetails.Add(orderDetail);
                    }

                    order.Total = orderTotal;

                    db.SaveChanges();

                    EmptyCart();

                    return order.Id;
               
            }
            catch (Exception ex)
            {
                throw new Exception("Ha ocurrido un problema al crear el pedido" + ex);
            }
        }

        /// <summary>
        /// Método que vacía un carrito de la compra cando esta se efectua
        /// </summary>
        public void EmptyCart()
        {
            var cartItems = db.ShoppingCarts.Where(c => c.CartId == ShoppingCartId);

            foreach (var cartItem in cartItems)
            {
                db.ShoppingCarts.Remove(cartItem);
            }
            db.SaveChanges();
        }
    }
}