﻿using System.Linq;
using E_Shop.CORE.Domain.Classes;

namespace E_Shop.CORE.Contracts
{
    /// <summary>
    /// Interfaz de los métodos de los comentarios de una variedad
    /// </summary>
    public interface ICommentManager : IManager<Comment>
    {
        IQueryable<Comment> GetAllByVarietyId(int varietyId);
    }
}