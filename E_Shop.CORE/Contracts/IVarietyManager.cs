﻿using System.Linq;
using E_Shop.CORE.Domain.Classes;

namespace E_Shop.CORE.Contracts
{
    /// <summary>
    /// Interfaz de los métodos de las variedades
    /// </summary>
    public interface IVarietyManager : IManager<Variety>
    {
        IQueryable<Variety> GetAllByProductId(int? productid);

        IQueryable<Variety> GetAllOnSale();

        IQueryable<Variety> GetAllByColour(string colour);

        Variety GetByProductId(int productid);

        Variety GetByPermalink(string permalink);

        IQueryable<Variety> GetAllByProductBrand(string brand);

        IQueryable<Variety> GetAllByProductFamily(int familyId);

        IQueryable<Variety> GetAllByProductPriceRange(decimal price1, decimal price2);
    }
}