﻿using E_Shop.CORE.Domain.Classes;

namespace E_Shop.CORE.Contracts
{
    /// <summary>
    /// Interfaz de métodos de los items del carrito
    /// </summary>
    public interface ICartItemManager : IManager<CartItem>
    {
    }
}