﻿using E_Shop.CORE.Domain.Classes;

namespace E_Shop.CORE.Contracts
{
    /// <summary>
    /// Interfaz de los métodos de los usuarios
    /// </summary>
    public interface IUserManager: IManager<ApplicationUser>
    {
        ApplicationUser GetByUserName(string name);
    }
}