﻿using E_Shop.CORE.Domain.Classes;

namespace E_Shop.CORE.Contracts
{
    /// <summary>
    /// Interfaz de los métodos de los métodos de pago
    /// </summary>
    public interface IMethodPayment
    {
        string name { get; }

        void Payment(Order order);

        void RequestOK(int orderId);

        void RequestKO(int orderId);
    }
}