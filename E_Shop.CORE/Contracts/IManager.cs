using System.Linq;

namespace E_Shop.CORE.Contracts
{
    /// <summary>
    /// Interfaz de los m�todos gen�ricos
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IManager<T>
        where T : class
    {
        IApplicationDbContext db { get; }

        IQueryable<T> GetAll(string include = null);

        T Add(T entity);

        T GetById(object[] key);

        T GetByName(string name);

        T GetById(int key);

        T Delete(T entity);

        void Edit(T entity, T newData);

        void Edit(int key, T newData);

        void Edit(object[] key, T newData);

        void Edit(string name, T newData);
    }
}