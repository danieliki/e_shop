﻿using System.Linq;
using E_Shop.CORE.Domain.Classes;

namespace E_Shop.CORE.Contracts
{
    /// <summary>
    /// Interfaz de los métodos de las líneas de un pedido
    /// </summary>
    public interface IOrderDetailManager : IManager<OrderDetail>
    {
        IQueryable<OrderDetail> GetAllByOrderId(int orderId);
    }
}