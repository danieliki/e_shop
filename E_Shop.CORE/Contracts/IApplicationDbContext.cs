using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Threading;
using System.Threading.Tasks;
using E_Shop.CORE.Domain.Classes;
using Microsoft.AspNet.Identity.EntityFramework;

namespace E_Shop.CORE.Contracts
{
    /// <summary>
    /// Interfaz del contexto de BBDD
    /// </summary>
    public interface IApplicationDbContext
    {
        DbSet<Address> Addresses { get; set; }

        DbSet<CartItem> CartItems { get; set; }

        DbSet<Comment> Comments { get; set; }

        DbSet<Family> Families { get; set; }

        DbSet<IVA> IVAs { get; set; }

        DbSet<Order> Orders { get; set; }

        DbSet<OrderDetail> OrderDetails { get; set; }

        DbSet<PaymentMethod> PaymentMethods { get; set; }

        DbSet<Product> Products { get; set; }

        DbSet<ShoppingCart> ShoppingCarts { get; set; }

        IDbSet<ApplicationUser> Users { get; set; }

        DbSet<Variety> Varieties { get; set; }

        IDbSet<IdentityRole> Roles { get; set; }

        bool RequireUniqueEmail { get; set; }

        Database Database { get; }

        DbChangeTracker ChangeTracker { get; }

        DbContextConfiguration Configuration { get; }

        DbSet Set(Type entityType);

        int SaveChanges();

        Task<int> SaveChangesAsync();

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);

        IEnumerable<DbEntityValidationResult> GetValidationErrors();

        DbEntityEntry Entry(object entity);

        void Dispose();

        string ToString();

        bool Equals(object obj);

        int GetHashCode();

        Type GetType();

        DbSet<TEntity> Set<TEntity>() where TEntity : class;
    }
}