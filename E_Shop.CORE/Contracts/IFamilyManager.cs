﻿using System.Linq;
using E_Shop.CORE.Domain.Classes;

namespace E_Shop.CORE.Contracts
{
    /// <summary>
    /// Interfaz de los métodos de las familias de los productos
    /// </summary>
    public interface IFamilyManager : IManager<Family>
    {
        Family GetByParentId(int parentId);

        IQueryable<Family> GetAllByParentId(int parentId);
    }
}