using E_Shop.CORE.Domain.Classes;

namespace E_Shop.CORE.Contracts
{
    /// <summary>
    /// Interfaz de m�todos de los carriots de la compra
    /// </summary>
    public interface IShoppingCartManager : IManager<ShoppingCart>
    {
    }
}