﻿using E_Shop.CORE.Domain.Classes;

namespace E_Shop.CORE.Contracts
{
    /// <summary>
    /// Interfaz de los métodos de los IVAS 
    /// </summary>
    public interface IIVAManager : IManager<IVA>
    {
         
    }
}