using System.Linq;
using E_Shop.CORE.Domain.Classes;

namespace E_Shop.CORE.Contracts
{
    /// <summary>
    /// Interfaz de los m�todos de los productos
    /// </summary>
    public interface IProductManager : IManager<Product>
    {
        IQueryable<Product> GetAllByBrand(string brand);

        IQueryable<Product> GetAllByPrice(decimal price1, decimal price2);

        IQueryable<Product> GetAllByFamily(int familyId);
    }
}