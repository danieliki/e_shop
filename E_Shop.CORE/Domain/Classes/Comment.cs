﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace E_Shop.CORE.Domain.Classes
{
    /// <summary>
    /// Clase de comentarios que el usuario puede hacer acerca del un producto
    /// </summary>
    public class Comment
    {
        /// <summary>
        /// Id del comentario
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Texto del comentario
        /// </summary>
        [Required]
        [MaxLength(250)]
        [DataType(DataType.MultilineText)]
        public string Text { get; set; }

        /// <summary>
        /// Fecha del comentario
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateTime { get; set; }

        /// <summary>
        /// Id de la variedad a a que pertenece
        /// </summary>
        [ForeignKey("Variety")]
        public int VarietyId { get; set; }

        /// <summary>
        /// Variedad a la que pertenece el comentario
        /// </summary>
        public Variety Variety { get; set; }

        /// <summary>
        /// Usuario que ha escrito el comentario
        /// </summary>
        public string User { get; set; }
    }
}