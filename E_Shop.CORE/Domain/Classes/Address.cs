﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace E_Shop.CORE.Domain.Classes
{
    /// <summary>
    /// Clase de las direcciones del usuario
    /// </summary>
    public class Address
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Direccion del envio
        /// </summary>
        /// 
        [MaxLength(50)]
        public string Dir { get; set; }

        /// <summary>
        /// Codigo postal del envio
        /// </summary>
        public int PostalCode { get; set; }

        /// <summary>
        /// Ciudad del envio
        /// </summary>
        [MaxLength(50)]
        public string City { get; set; }

        /// <summary>
        /// Provincia del envio
        /// </summary>
        public string State { get; set; }
    }
}