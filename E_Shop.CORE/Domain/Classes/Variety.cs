﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace E_Shop.CORE.Domain.Classes
{
    /// <summary>
    /// Clase que representa la variedad de un producto
    /// </summary>
    public class Variety
    {
        /// <summary>
        /// Identificador de la variedad
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Campo que quita simbolos de los nombres para mejorar el posicionamiento web
        /// </summary>
        [MaxLength(100)]
        public string Permalink { get; set; }

        /// <summary>
        /// Fecha desde la que se tiene en tienda la variedad
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DateFrom { get; set; }

        /// <summary>
        /// Propiedad que especifica si la variedad esta en oferta o no
        /// </summary>
        public bool OnSale { get; set; }

        /// <summary>
        /// Unidades de la variedad
        /// </summary>
        [Required]
        public int Stock { get; set; }

        /// <summary>
        /// Color de la variedad
        /// </summary>
        [Required]
        public string Colour { get; set; }

        /// <summary>
        /// Peso de la variedad
        /// </summary>
        [Required]
        public decimal Weight { get; set; }

        /// <summary>
        /// Material de la variedad
        /// </summary>
        [Required]
        public string Material { get; set; }

        /// <summary>
        /// Cantidad elegida de una variedad
        /// </summary>
        public int Quantity { get; set; }

        #region Producto
        /// <summary>
        /// Identificador del producto al que pertenece la variedad
        /// </summary>
        [ForeignKey("Product")]
        [Required]
        public int ProductId { get; set; }

        /// <summary>
        /// Producto al que pertenece la variedad
        /// </summary>
        public virtual Product Product { get; set; }
        #endregion
    }
}