﻿namespace E_Shop.CORE.Domain.Classes
{
    /// <summary>
    /// Clase de los métodos de pago
    /// </summary>
    public class PaymentMethod
    {
        /// <summary>
        /// Identificador del método de pago
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Nombre del método de pago
        /// </summary>
        public string Name { get; set; }
    }
}