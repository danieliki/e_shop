﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace E_Shop.CORE.Domain.Classes
{
    /// <summary>
    /// Clase que representa el orden jerarquico de los productos
    /// </summary>
    public class Family 
    {
        /// <summary>
        /// Identificador de la familia
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Nombre de la familia
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Identificador de la familia padre
        /// </summary>
        [ForeignKey("Parent")]
        public int? ParentId { get; set; }

        /// <summary>
        /// Familia padre a la que pertenece
        /// </summary>
        public Family Parent { get; set; }
    }
}