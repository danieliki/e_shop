﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using E_Shop.CORE.Domain.Enums;

namespace E_Shop.CORE.Domain.Classes
{
    /// <summary>
    /// Clase que representa un pedido
    /// </summary>
    public class Order
    {
        /// <summary>
        /// Identificador del pedido
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Fecha del pedido
        /// </summary>
        public DateTime OrderDate { get; set; }

        /// <summary>
        /// Usuario que ha hecho el pedido
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// Precio total del pedido
        /// </summary>
        public decimal Total { get; set; }

        /// <summary>
        /// Dirección de envío del pedido
        /// </summary>
        public Address ShippingAddress { get; set; }

        /// <summary>
        /// Dirección de facturación del pedido
        /// </summary>
        public Address BillingAddress { get; set; }

        /// <summary>
        /// Enumerado que indica la forma de pago del pedido
        /// </summary>
        public PaymentMethod PaymentMethod { get; set; }

        /// <summary>
        /// Enumerado que indica el estado del pedido
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Enumerado que indica si el pedido esta pagado o no
        /// </summary>
        public bool IsPaid { get; set; }

        /// <summary>
        /// Numero de seguimiento del pedido
        /// </summary>
        public Guid TrackingNumber { get; set; }

        #region Navigation properties
        /// <summary>
        /// Lista de líneas de pedido que componen el pedido
        /// </summary>
        public IList<OrderDetail> OrderDetails { get; set; }
        #endregion
    }
}
