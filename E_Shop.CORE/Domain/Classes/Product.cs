﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace E_Shop.CORE.Domain.Classes
{
    /// <summary>
    /// Clase que representa un producto de la tienda
    /// </summary>
    [Serializable]
    public class Product
    {
        /// <summary>
        /// Id del producto
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Nombre completo del producto
        /// </summary>
        public string FullName
        {
            get
            {
                return Brand + " - " + Model;
            }
        }

        /// <summary>
        /// Marca del producto
        /// </summary>
        [Required]
        public string Brand { get; set; }

        /// <summary>
        /// Modelo del producto
        /// </summary>
        [Required]
        public string Model { get; set; }

        /// <summary>
        /// Precio del producto
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        [Required]
        public decimal Price { get; set; }

        /// <summary>
        /// Descripción del producto
        /// </summary>
        public string Description { get; set; }

        #region IVA
        /// <summary>
        /// Iva que se aplica al precio del producto
        /// </summary>        
        public virtual IVA IVA { get; set; }

        /// <summary>
        /// Identificador del iva aplicado al precio del producto
        /// </summary>
        [ForeignKey("IVA")]
        [Required]
        public int IVAId { get; set; }
        #endregion

        #region Familia
        /// <summary>
        /// Identificador de la familia a la que pertenece el producto
        /// </summary>
        [ForeignKey("Family")]
        [Required]
        public int? FamilyId { get; set; }

        /// <summary>
        /// Familia del producto
        /// </summary>
        public Family Family { get; set; }
        #endregion

        #region Navigation properties
        /// <summary>
        /// Colección de variedades del producto
        /// </summary>
        public List<Variety> Varieties { get; set; }

       
        #endregion
    }
}