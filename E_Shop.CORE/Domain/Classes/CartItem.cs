﻿using System.ComponentModel.DataAnnotations.Schema;

namespace E_Shop.CORE.Domain.Classes
{

    /// <summary>
    /// Clase de los item del carrito
    /// </summary>
    public class CartItem
    {
        /// <summary>
        /// Id del item del carrito
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Sesión del item del carrito
        /// </summary>
        public string Session { get; set; }

        /// <summary>
        /// Usuario al que pertenece el item del carrito
        /// </summary>
        public ApplicationUser User { get; set; }

        /// <summary>
        /// Id del usuario al que pertenece el item del carrito
        /// </summary>
        [ForeignKey("User")]
        public string UserId { get; set; }

        /// <summary>
        /// Variedad a la que pertenece el item del carrito
        /// </summary>
        public Variety Variety { get; set; }

        /// <summary>
        /// Id de la variedad a la que pertenece el item del carrito
        /// </summary>
        [ForeignKey("Variety")]
        public int VarietyId { get; set; }

        /// <summary>
        /// Cantidad de variedades que contiene el item del carrito
        /// </summary>
        public int Quantity { get; set; }
    }
}