﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace E_Shop.CORE.Domain.Classes
{
    /// <summary>
    /// Clase que representa el carrito de la compra
    /// </summary>
    public class ShoppingCart
    {
        /// <summary>
        /// Identificador del carrito
        /// </summary>
        [Key]
        public int RecordId { get; set; }

        /// <summary>
        /// Identificador del carrito para compras anónimas
        /// </summary>
        public string CartId { get; set; }

        /// <summary>
        /// Cantidad de productos que hay en el carrito
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Fecha del carrito
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Identificador del producto en el carrito
        /// </summary>
        [ForeignKey("Variety")]
        public int VarietyId { get; set; }

        /// <summary>
        /// Producto que hay en el carrito
        /// </summary>
        public virtual Variety Variety { get; set; }

        /// <summary>
        /// Imagen de la variedad de cada item del carrito
        /// </summary>
        public string Image
        {
            get
            {
                return "/Images/Product/" + BuildNameImage(VarietyId);
            }
            private set { }
        }

        /// <summary>
        /// Propiedad para generar el nombre de la imagen
        /// </summary>
        /// <param name="varietyId"></param>
        /// <returns></returns>
        private string BuildNameImage(int varietyId)
        {
            return $"{varietyId}_0.jpg";
        }
    }
}
