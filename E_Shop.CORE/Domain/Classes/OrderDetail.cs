﻿using System.ComponentModel.DataAnnotations;

namespace E_Shop.CORE.Domain.Classes
{
    /// <summary>
    /// Clase que representa una línea de pedido
    /// </summary>
    public class OrderDetail
    {
        /// <summary>
        /// Identificador de la línea de pedido
        /// </summary>
        [Key]
        public int OrderDetailId { get; set; }

        /// <summary>
        /// Cantidad de la variedad
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Identificador de la variedad de la línea 
        /// </summary>
        public int VarietyId { get; set; }

        /// <summary>
        /// Variedad de la línea 
        /// </summary>
        public Variety Variety { get; set; }

        #region Order
        /// <summary>
        /// Identificador del pedido al que pertenece la línea
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Pedido al que pertenece la línea
        /// </summary>
        public Order Order { get; set; }
        #endregion
    }
}
