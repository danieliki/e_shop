﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace E_Shop.CORE.Domain.Classes
{
    /// <summary>
    /// Clase que representa al usuario que va a comprar
    /// </summary>
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        /// <summary>
        /// Nombre propio del usuario
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Primer apellido del usuario
        /// </summary>
        public string SurName1 { get; set; }

        /// <summary>
        /// Segundo apellido del usuario
        /// </summary>
        public string SurName2 { get; set; }

        /// <summary>
        /// Colección de direcciones del usuario
        /// </summary>
        public ICollection<Address> Addresses { get; set; }

       
    }
}