﻿using System.ComponentModel.DataAnnotations;

namespace E_Shop.CORE.Domain.Classes
{
    /// <summary>
    /// Clase de IVA
    /// </summary>
    public class IVA
    {
        /// <summary>
        /// Identificador del IVA
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Nombre del IVA
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// Valor del IVA
        /// </summary>
        [Required]
        public decimal Value { get; set; }
    }
}