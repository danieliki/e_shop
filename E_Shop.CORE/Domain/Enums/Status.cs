﻿namespace E_Shop.CORE.Domain.Enums
{
    /// <summary>
    /// Enumerado que indica el estado del pedido
    /// </summary>
    public enum Status
    {
        Reserved = 0,
        Paid = 1,
        Send = 2,
        Recived = 3,
        Return = 4
    }
}