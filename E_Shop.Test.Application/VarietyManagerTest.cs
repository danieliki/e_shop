﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using E_Shop.Application;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace E_Shop.Test.Application
{
    [TestClass]
    public class VarietyManagerTest
    {

        IQueryable<Variety> _varieties = new List<Variety>
            {
                new Variety { Id = 1, OnSale = true, Stock = 4, Colour = "Rojo", Weight = 5, Material = "Fresno", ProductId = 2},
                new Variety { Id = 2, OnSale = true, Stock = 5, Colour = "Sunburst", Weight = 5, Material = "Arce", ProductId = 4},
                new Variety { Id = 3, OnSale = false, Stock = 3, Colour = "Negro", Weight = 4, Material = "Arce", ProductId = 4},
                new Variety { Id = 4, OnSale = true, Stock = 2, Colour = "Carbon", Weight = 4, Material = "Roble", ProductId = 6},
                new Variety { Id = 5, OnSale = false, Stock = 5, Colour = "Gris Oscuro", Weight = 3, Material = "Roble", ProductId = 7}
            }.AsQueryable();

        [TestMethod]
        public void TestGetAllByProductId()
        {

            //Arrange
            var mockSetVariety = new Mock<DbSet<Variety>>();
            mockSetVariety.As<IQueryable<Variety>>().Setup(m => m.Provider).Returns(_varieties.Provider);
            mockSetVariety.As<IQueryable<Variety>>().Setup(m => m.Expression).Returns(_varieties.Expression);
            mockSetVariety.As<IQueryable<Variety>>().Setup(m => m.ElementType).Returns(_varieties.ElementType);
            mockSetVariety.As<IQueryable<Variety>>().Setup(m => m.GetEnumerator()).Returns(_varieties.GetEnumerator());

            var mockContext = new Mock<IApplicationDbContext>();
            mockContext.Setup(c => c.Varieties).Returns(mockSetVariety.Object);
            mockContext.Setup(c => c.Set<Variety>()).Returns(mockSetVariety.Object);

            //Act
            IVarietyManager varietyManager = new VarietyManager(mockContext.Object);
            IQueryable<Variety> resultVariety = varietyManager.GetAllByProductId(4);
            IQueryable<Variety> resultVariety2 = varietyManager.GetAllByProductId(6);

            //Assert
            Assert.IsTrue(resultVariety.Count() == 2);
            Assert.IsFalse(resultVariety.Count() == 3);
            Assert.AreNotEqual(resultVariety, resultVariety2);
            Assert.IsNotNull(resultVariety);
            Assert.IsNotNull(resultVariety2);
        }

        [TestMethod]
        public void TestGetAllOnSale()
        {
            //Arrange
            var mockSetVariety = new Mock<DbSet<Variety>>();
            mockSetVariety.As<IQueryable<Variety>>().Setup(m => m.Provider).Returns(_varieties.Provider);
            mockSetVariety.As<IQueryable<Variety>>().Setup(m => m.Expression).Returns(_varieties.Expression);
            mockSetVariety.As<IQueryable<Variety>>().Setup(m => m.ElementType).Returns(_varieties.ElementType);
            mockSetVariety.As<IQueryable<Variety>>().Setup(m => m.GetEnumerator()).Returns(_varieties.GetEnumerator());

            var mockContext = new Mock<IApplicationDbContext>();
            mockContext.Setup(c => c.Varieties).Returns(mockSetVariety.Object);
            mockContext.Setup(c => c.Set<Variety>()).Returns(mockSetVariety.Object);

            //Act
            IVarietyManager varietyManager = new VarietyManager(mockContext.Object);
            IQueryable<Variety> resultVariety = varietyManager.GetAllOnSale();

            //Assert
            Assert.IsTrue(resultVariety.Count() == 3);
            Assert.IsFalse(resultVariety.Count() == 2);
            Assert.IsNotNull(resultVariety);
        }

        [TestMethod]
        public void TestGetAllByColour()
        {
            //Arrange
            var mockSetVariety = new Mock<DbSet<Variety>>();
            mockSetVariety.As<IQueryable<Variety>>().Setup(m => m.Provider).Returns(_varieties.Provider);
            mockSetVariety.As<IQueryable<Variety>>().Setup(m => m.Expression).Returns(_varieties.Expression);
            mockSetVariety.As<IQueryable<Variety>>().Setup(m => m.ElementType).Returns(_varieties.ElementType);
            mockSetVariety.As<IQueryable<Variety>>().Setup(m => m.GetEnumerator()).Returns(_varieties.GetEnumerator());

            var mockContext = new Mock<IApplicationDbContext>();
            mockContext.Setup(c => c.Varieties).Returns(mockSetVariety.Object);
            mockContext.Setup(c => c.Set<Variety>()).Returns(mockSetVariety.Object);

            //Act
            IVarietyManager varietyManager = new VarietyManager(mockContext.Object);
            IQueryable<Variety> resultVariety = varietyManager.GetAllByColour("Rojo");
            IQueryable<Variety> resultVariety3 = varietyManager.GetAllByColour("Negro");

            //Assert
            Assert.IsTrue(resultVariety.Count() == 1);
            Assert.IsFalse(resultVariety.Count() == 3);
            Assert.IsNotNull(resultVariety);
            Assert.IsNotNull(resultVariety3);
            Assert.AreNotEqual(resultVariety, resultVariety3);
        }

        [TestMethod]
        public void TestGetByProductId()
        {
            //Arrange
            var mockSetVariety = new Mock<DbSet<Variety>>();
            mockSetVariety.As<IQueryable<Variety>>().Setup(m => m.Provider).Returns(_varieties.Provider);
            mockSetVariety.As<IQueryable<Variety>>().Setup(m => m.Expression).Returns(_varieties.Expression);
            mockSetVariety.As<IQueryable<Variety>>().Setup(m => m.ElementType).Returns(_varieties.ElementType);
            mockSetVariety.As<IQueryable<Variety>>().Setup(m => m.GetEnumerator()).Returns(_varieties.GetEnumerator());

            var mockContext = new Mock<IApplicationDbContext>();
            mockContext.Setup(c => c.Varieties).Returns(mockSetVariety.Object);
            mockContext.Setup(c => c.Set<Variety>()).Returns(mockSetVariety.Object);

            //Act
            IVarietyManager varietyManager = new VarietyManager(mockContext.Object);
            Variety resultVariety = varietyManager.GetByProductId(4);

            //Assert
            Assert.IsTrue(resultVariety.ProductId == 4);
            Assert.IsFalse(resultVariety.ProductId == 1);
            Assert.IsNotNull(resultVariety);
            Assert.AreEqual(resultVariety.Material, "Arce");
            Assert.AreNotEqual(resultVariety.Material, "Fresno");
            Assert.AreEqual(resultVariety.Stock, 5);
            Assert.AreNotEqual(resultVariety.Stock, 3);
            Assert.IsTrue(resultVariety.Id == 2);
            Assert.IsFalse(resultVariety.Id == 1);
        }
    }
}