﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using E_Shop.Application;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace E_Shop.Test.Application
{
    [TestClass]
    public class ProductManagerTest
    {
        IQueryable<Product> _products = new List<Product>
            {
                new Product { Id = 1, Brand = "Fender", Model = "Stratocaster", Price = 1250, FamilyId = 6, IVAId = 1},
                new Product { Id = 2, Brand = "Gibson", Model = "Les Paul Standard", Price = 1450, FamilyId = 6, IVAId = 1},
                new Product { Id = 8, Brand = "Epiphone", Model = "EJ", Price = 850, FamilyId = 7, IVAId = 1},
                new Product { Id = 12, Brand = "Gretsch", Model = "G5", Price = 480, FamilyId = 7, IVAId = 1},
                new Product { Id = 22, Brand = "Maxon", Model = "OD808", Price = 90, FamilyId = 28, IVAId = 1},
            }.AsQueryable();

        [TestMethod]
        public void TestGetAllByBrand()
        {
            //Arrange
            var mockSetProduct = new Mock<DbSet<Product>>();
            mockSetProduct.As<IQueryable<Product>>().Setup(m => m.Provider).Returns(_products.Provider);
            mockSetProduct.As<IQueryable<Product>>().Setup(m => m.Expression).Returns(_products.Expression);
            mockSetProduct.As<IQueryable<Product>>().Setup(m => m.ElementType).Returns(_products.ElementType);
            mockSetProduct.As<IQueryable<Product>>().Setup(m => m.GetEnumerator()).Returns(_products.GetEnumerator());

            var mockContext = new Mock<IApplicationDbContext>();
            mockContext.Setup(c => c.Products).Returns(mockSetProduct.Object);
            mockContext.Setup(c => c.Set<Product>()).Returns(mockSetProduct.Object);

            //Act
            IProductManager productManager = new ProductManager(mockContext.Object);
            IQueryable<Product> resultProduct = productManager.GetAllByBrand("Gibson");
            IQueryable<Product> resultProduct2 = productManager.GetAllByBrand("Maxon");

            //Assert
            Assert.IsTrue(resultProduct.Count() == 1);
            Assert.IsFalse(resultProduct.Count() == 3);
            Assert.IsTrue(resultProduct2.Count() == 1);
            Assert.IsFalse(resultProduct2.Count() == 2);
            Assert.AreNotEqual(resultProduct, resultProduct2);
            Assert.IsNotNull(resultProduct);
            Assert.IsNotNull(resultProduct2);
        }

        [TestMethod]
        public void TestGetAllByPrice()
        {
            //Arrange
            var mockSetProduct = new Mock<DbSet<Product>>();
            mockSetProduct.As<IQueryable<Product>>().Setup(m => m.Provider).Returns(_products.Provider);
            mockSetProduct.As<IQueryable<Product>>().Setup(m => m.Expression).Returns(_products.Expression);
            mockSetProduct.As<IQueryable<Product>>().Setup(m => m.ElementType).Returns(_products.ElementType);
            mockSetProduct.As<IQueryable<Product>>().Setup(m => m.GetEnumerator()).Returns(_products.GetEnumerator());

            var mockContext = new Mock<IApplicationDbContext>();
            mockContext.Setup(c => c.Products).Returns(mockSetProduct.Object);
            mockContext.Setup(c => c.Set<Product>()).Returns(mockSetProduct.Object);

            //Act
            IProductManager productManager = new ProductManager(mockContext.Object);
            IQueryable<Product> resultProduct = productManager.GetAllByPrice(50, 400);
            IQueryable<Product> resultProduct2 = productManager.GetAllByPrice(1000, 2000);

            //Assert
            Assert.IsTrue(resultProduct.Count() == 1);
            Assert.IsFalse(resultProduct.Count() == 3);
            Assert.IsTrue(resultProduct2.Count() == 2);
            Assert.IsFalse(resultProduct2.Count() == 3);
            Assert.AreNotEqual(resultProduct, resultProduct2);
            Assert.IsNotNull(resultProduct);
            Assert.IsNotNull(resultProduct2);
        }

        [TestMethod]
        public void TestGetAllByFamily()
        {
            //Arrange
            var mockSetProduct = new Mock<DbSet<Product>>();
            mockSetProduct.As<IQueryable<Product>>().Setup(m => m.Provider).Returns(_products.Provider);
            mockSetProduct.As<IQueryable<Product>>().Setup(m => m.Expression).Returns(_products.Expression);
            mockSetProduct.As<IQueryable<Product>>().Setup(m => m.ElementType).Returns(_products.ElementType);
            mockSetProduct.As<IQueryable<Product>>().Setup(m => m.GetEnumerator()).Returns(_products.GetEnumerator());

            var mockContext = new Mock<IApplicationDbContext>();
            mockContext.Setup(c => c.Products).Returns(mockSetProduct.Object);
            mockContext.Setup(c => c.Set<Product>()).Returns(mockSetProduct.Object);

            //Act
            IProductManager productManager = new ProductManager(mockContext.Object);
            IQueryable<Product> resultProduct = productManager.GetAllByFamily(6);
            IQueryable<Product> resultProduct2 = productManager.GetAllByFamily(7);

            //Assert
            Assert.IsTrue(resultProduct.Count() == 2);
            Assert.IsFalse(resultProduct.Count() == 3);
            Assert.IsTrue(resultProduct2.Count() == 2);
            Assert.IsFalse(resultProduct2.Count() == 3);
            Assert.AreNotEqual(resultProduct, resultProduct2);
            Assert.IsNotNull(resultProduct);
            Assert.IsNotNull(resultProduct2);
        }
    }
}
