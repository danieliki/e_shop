﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using E_Shop.Application;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace E_Shop.Test.Application
{
    [TestClass]
    public class CommentManagerTest
    {
        IQueryable<Comment> _comments = new List<Comment>
            {
                new Comment { Id = 7, Text = "Muy buena", VarietyId = 38, User = "admin@eshop.com"},
                new Comment { Id = 8, Text = "Fabulosa", VarietyId = 2, User = "admin@eshop.com"},
                new Comment { Id = 9, Text = "Una guitarra polivalente", VarietyId = 1, User = "admin@eshop.com"}
            }.AsQueryable();


        [TestMethod]
        public void GetAllByVariertyId()
        {
            //Arrange
            var mockSetProduct = new Mock<DbSet<Comment>>();
            mockSetProduct.As<IQueryable<Comment>>().Setup(m => m.Provider).Returns(_comments.Provider);
            mockSetProduct.As<IQueryable<Comment>>().Setup(m => m.Expression).Returns(_comments.Expression);
            mockSetProduct.As<IQueryable<Comment>>().Setup(m => m.ElementType).Returns(_comments.ElementType);
            mockSetProduct.As<IQueryable<Comment>>().Setup(m => m.GetEnumerator()).Returns(_comments.GetEnumerator());

            var mockContext = new Mock<IApplicationDbContext>();
            mockContext.Setup(c => c.Comments).Returns(mockSetProduct.Object);
            mockContext.Setup(c => c.Set<Comment>()).Returns(mockSetProduct.Object);

            //Act
            ICommentManager commentManager = new CommentManager(mockContext.Object);
            IQueryable<Comment> resultComment = commentManager.GetAllByVarietyId(1);
            IQueryable<Comment> resultComment2 = commentManager.GetAllByVarietyId(38);

            //Assert
            Assert.IsTrue(resultComment.Count() == 1);
            Assert.IsFalse(resultComment.Count() == 3);
            Assert.IsTrue(resultComment2.Count() == 1);
            Assert.IsFalse(resultComment2.Count() == 2);
            Assert.AreNotEqual(resultComment, resultComment2);
            Assert.IsNotNull(resultComment);
            Assert.IsNotNull(resultComment2);
        }
    }
}
