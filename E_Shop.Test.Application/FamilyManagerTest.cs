﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using E_Shop.Application;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace E_Shop.Test.Application
{
    [TestClass]
    public class FamilyManagerTest
    {
        IQueryable<Family> _families = new List<Family>
            {
                new Family { Id = 1, Name = "Instrumento", ParentId = null},
                new Family { Id = 2, Name = "Guitarra", ParentId = 1},
                new Family { Id = 3, Name = "Bajo", ParentId = 1},
                new Family { Id = 6, Name = "Guitarra Eléctrica", ParentId = 2},
                new Family { Id = 8, Name = "Bajo Eléctrico", ParentId = 3}
            }.AsQueryable();

        [TestMethod]
        public void GetAllByParentId()
        {
            //Arrange
            var mockSetFamily = new Mock<DbSet<Family>>();
            mockSetFamily.As<IQueryable<Family>>().Setup(m => m.Provider).Returns(_families.Provider);
            mockSetFamily.As<IQueryable<Family>>().Setup(m => m.Expression).Returns(_families.Expression);
            mockSetFamily.As<IQueryable<Family>>().Setup(m => m.ElementType).Returns(_families.ElementType);
            mockSetFamily.As<IQueryable<Family>>().Setup(m => m.GetEnumerator()).Returns(_families.GetEnumerator());

            var mockContext = new Mock<IApplicationDbContext>();
            mockContext.Setup(c => c.Families).Returns(mockSetFamily.Object);
            mockContext.Setup(c => c.Set<Family>()).Returns(mockSetFamily.Object);

            //Act
            IFamilyManager familyManager = new FamilyManager(mockContext.Object);
            IQueryable<Family> resultFamily = familyManager.GetAllByParentId(1);
            IQueryable<Family> resultFamily2 = familyManager.GetAllByParentId(2);

            //Assert
            Assert.IsTrue(resultFamily.Count() == 2);
            Assert.IsFalse(resultFamily.Count() == 3);
            Assert.IsTrue(resultFamily2.Count() == 1);
            Assert.IsFalse(resultFamily2.Count() == 2);
            Assert.AreNotEqual(resultFamily, resultFamily2);
            Assert.IsNotNull(resultFamily);
            Assert.IsNotNull(resultFamily2);
        }

        [TestMethod]
        public void GetByParentId()
        {
            //Arrange
            var mockSetFamily = new Mock<DbSet<Family>>();
            mockSetFamily.As<IQueryable<Family>>().Setup(m => m.Provider).Returns(_families.Provider);
            mockSetFamily.As<IQueryable<Family>>().Setup(m => m.Expression).Returns(_families.Expression);
            mockSetFamily.As<IQueryable<Family>>().Setup(m => m.ElementType).Returns(_families.ElementType);
            mockSetFamily.As<IQueryable<Family>>().Setup(m => m.GetEnumerator()).Returns(_families.GetEnumerator());

            var mockContext = new Mock<IApplicationDbContext>();
            mockContext.Setup(c => c.Families).Returns(mockSetFamily.Object);
            mockContext.Setup(c => c.Set<Family>()).Returns(mockSetFamily.Object);

            //Act
            IFamilyManager familyManager = new FamilyManager(mockContext.Object);
            Family resultFamily = familyManager.GetByParentId(1);
            Family resultFamily2 = familyManager.GetByParentId(2);

            //Assert
            Assert.AreNotEqual(resultFamily, resultFamily2);
            Assert.AreNotEqual(_families.First(), resultFamily);
            Assert.AreNotEqual(_families.First(), resultFamily2);
            Assert.IsNotNull(resultFamily);
            Assert.IsNotNull(resultFamily2);
            Assert.AreEqual(resultFamily.Id, 2);
            Assert.AreEqual(resultFamily2.Id, 6);
        }
    }
}
