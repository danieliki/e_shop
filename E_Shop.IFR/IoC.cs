﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.Unity;

namespace E_Shop.IFR
{
    /// <summary>
    /// Clase que genera la inyección de dependencias
    /// </summary>
    public class IoC
    {
        protected IUnityContainer container;

        #region Singleton
        private static readonly IoC current = new IoC();

        public static IoC Current
        {
            get
            {
                return current;
            }
        }
        #endregion

        #region metodos publicos
        public T Resolve<T>()
        {
            return container.Resolve<T>();
        }

        public T Resolve<T>(string name)
        {
            return container.Resolve<T>(name);
        }

        public object Resolve(Type t)
        {
            return container.Resolve(t);
        }

        public object Resolve(Type t, string name)
        {
            return container.Resolve(t, name);
        }

        public object ResolveAll<T>()
        {
            return container.ResolveAll<T>();
        }

        public IEnumerable<object> ResolveAll(Type type)
        {
            return container.ResolveAll(type);
        }
        #endregion

       

        #region Constructores
        static IoC()
        {
        }

        protected IoC()
        {
            container = new UnityContainer();
            container.RegisterType(
                GetType("E_Shop.CORE.Contracts.IApplicationDbContext, E_Shop.CORE"),
                GetType("E_Shop.DAL.ApplicationDbContext, E_Shop.DAL"),
                new ContainerControlledLifetimeManager());
            container.RegisterType(
                GetType("E_Shop.CORE.Contracts.IProductManager, E_Shop.CORE"),
                GetType("E_Shop.Application.ProductManager, E_Shop.Application"),
                new TransientLifetimeManager());
            container.RegisterType(
                GetType("E_Shop.CORE.Contracts.IFamilyManager, E_Shop.CORE"),
                GetType("E_Shop.Application.FamilyManager, E_Shop.Application"),
                new TransientLifetimeManager());
            container.RegisterType(
                GetType("E_Shop.CORE.Contracts.IIVAManager, E_Shop.CORE"),
                GetType("E_Shop.Application.IVAManager, E_Shop.Application"),
                new TransientLifetimeManager());
            container.RegisterType(
                GetType("E_Shop.CORE.Contracts.IShoppingCartManager, E_Shop.CORE"),
                GetType("E_Shop.Application.ShoppingCartManager, E_Shop.Application"),
                new TransientLifetimeManager());
            container.RegisterType(
                GetType("E_Shop.CORE.Contracts.IVarietyManager, E_Shop.CORE"),
                GetType("E_Shop.Application.VarietyManager, E_Shop.Application"),
                new TransientLifetimeManager());
            container.RegisterType(
                GetType("E_Shop.CORE.Contracts.IOrderManager, E_Shop.CORE"),
                GetType("E_Shop.Application.OrderManager, E_Shop.Application"),
                new TransientLifetimeManager());
            container.RegisterType(
                GetType("E_Shop.CORE.Contracts.ICommentManager, E_Shop.CORE"),
                GetType("E_Shop.Application.CommentManager, E_Shop.Application"),
                new TransientLifetimeManager());
            container.RegisterType(
                GetType("E_Shop.CORE.Contracts.IOrderDetailManager, E_Shop.CORE"),
                GetType("E_Shop.Application.OrderDetailManager, E_Shop.Application"),
                new TransientLifetimeManager());
            container.RegisterType(
                GetType("E_Shop.CORE.Contracts.IUserManager, E_Shop.CORE"),
                GetType("E_Shop.Application.UserManager, E_Shop.Application"),
                new TransientLifetimeManager());
        }

        private Type GetType(string typeString)
        {
            Type type = Type.GetType(typeString);
            if (type == null)
            {
                throw new Exception("El tipo no se ha podido resolver: " + typeString);
            }
            return type;
        }
        #endregion
    }
}