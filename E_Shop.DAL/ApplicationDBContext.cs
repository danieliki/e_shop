﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;
using Microsoft.AspNet.Identity.EntityFramework;

namespace E_Shop.DAL
{
    /// <summary>
    /// Clase de contexto de datos de la aplicacion
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityRole>().HasKey(r => r.Id).Property(p => p.Name).IsRequired();
            modelBuilder.Entity<IdentityUserRole>().HasKey(r => new { r.RoleId, r.UserId });
            modelBuilder.Entity<IdentityUserLogin>().HasKey(u => new { u.UserId, u.LoginProvider, u.ProviderKey});
            modelBuilder.Entity<Variety>()
                .Property(v => v.Permalink)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute()));
            base.OnModelCreating(modelBuilder);
        }

        /// <summary>
        /// Colección de datos persistibles de direcciones
        /// </summary>
        public DbSet<Address> Addresses { get; set; }

        /// <summary>
        /// Colección de datos persistibles de Artículos del carrito de la compra
        /// </summary>
        public DbSet<CartItem> CartItems { get; set; }

        /// <summary>
        /// Colección de datos persistibles de Comentrios
        /// </summary>
        public DbSet<Comment> Comments { get; set; }

        /// <summary>
        /// Colección de datos persistibles de Familias
        /// </summary>
        public DbSet<Family> Families { get; set; }

        /// <summary>
        /// Colección de datos persistibles de IVAs
        /// </summary>
        public DbSet<IVA> IVAs { get; set; }

        /// <summary>
        /// Colección de datos persistibles de Pedidos
        /// </summary>
        public DbSet<Order> Orders { get; set; }

        /// <summary>
        /// Colección de datos persistibles de líneas de Pedido
        /// </summary>
        public DbSet<OrderDetail> OrderDetails { get; set; }

        /// <summary>
        /// Colección de datos persistibles de métodos de pago
        /// </summary>
        public DbSet<PaymentMethod> PaymentMethods { get; set; }

        /// <summary>
        /// Colección de datos persistibles de Productos
        /// </summary>
        public DbSet<Product> Products { get; set; }

        /// <summary>
        /// Colección de datos persistibles de Carritos de la compra
        /// </summary>
        public DbSet<ShoppingCart> ShoppingCarts { get; set; }

        /// <summary>
        /// Colección de datos persistibles de variedades del producto
        /// </summary>
        public DbSet<Variety> Varieties { get; set; }

        
    }
}