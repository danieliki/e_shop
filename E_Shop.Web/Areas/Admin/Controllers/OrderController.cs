﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;
using E_Shop.CORE.Domain.Enums;
using E_Shop.Web.Areas.Admin.Models.Order;
using E_Shop.Web.JQueryDataTable;
using RestSharp;

namespace E_Shop.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Controlador de los pedidos - Solo accesible con el rol Admin
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class OrderController : Controller
    {
        private IOrderManager orderManager;

        /// <summary>
        /// Constructor del controlador
        /// </summary>
        /// <param name="orderManager"></param>
        public OrderController(IOrderManager orderManager)
        {
            this.orderManager = orderManager;
        }

        /// <summary>
        /// Método que devuelve todos los pedidos en formato JSon para pintar una tabla con Ajax
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexData()
        {
            JQueryDataTableRequest request = new JQueryDataTableRequest(System.Web.HttpContext.Current.Request);
            var model =
                 orderManager.GetAll()
                     .Select(e => new OrderIndexViewModel()
                     {
                         Id = e.Id,
                         User = e.User,
                         Date = e.OrderDate,
                         PaymentMethod = e.PaymentMethod.Name,
                         IsPaid = e.IsPaid,
                         Status = e.Status.ToString(),
                         TrakingNumber = e.TrackingNumber.ToString()
                     });

            JQueryDataTableResponse result = JQueryHelper.CreateJQueryDataTableResponse(model, request);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que muestra una tabla con todos los pedidos
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var model =
                orderManager.GetAll()
                    .Select(e => new OrderIndexViewModel()
                    {
                        Id = e.Id,
                        User = e.User,
                        Date = e.OrderDate,
                        PaymentMethod = e.PaymentMethod.Name,
                        IsPaid = e.IsPaid,
                        Status = e.Status.ToString(),
                        TrakingNumber = e.TrackingNumber.ToString()
                    });

            return View(model);
        }

        /// <summary>
        /// Método que pinta la vista de edición de un pedido
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            OrderCreateViewModel model = new OrderCreateViewModel();
            model = PrepareViewModel(model);
            model.Order = orderManager.GetById(id);

            return View(model);
        }

        /// <summary>
        /// Método de edición de un pedido
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(int id, OrderCreateViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    orderManager.Edit(id, model.Order);
                    orderManager.db.SaveChanges();
                    return RedirectToAction("Index");
                }
                model = PrepareViewModel(model);
                return View(model);
            }
            catch (Exception)
            {
                return View(model);
            }
        }

        /// <summary>
        /// Método que prepara los dropdownlist de la vista
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private OrderCreateViewModel PrepareViewModel(OrderCreateViewModel model)
        {
            IEnumerable<Status> status = Enum.GetValues(typeof(Status)).Cast<Status>();
            model.Status = from s in status
                           select new SelectListItem()
                {
                    Text = s.ToString(),
                    Value = ((int)s).ToString()
                };
           
            return model;
        }

        /// <summary>
        /// Método que devuelve los resultados del web service
        /// </summary>
        /// <returns></returns>
        public ActionResult TestService()
        {
            var client = new RestClient("http://localhost:65371/");
            var request = new RestRequest("api/Order", Method.GET) { RequestFormat = DataFormat.Json };
            var response = client.Execute<List<Order>>(request);

            if (response.Data == null)
            {
                throw new Exception(response.ErrorMessage);
            }
            return View(response.Data);
        }
    }
}
