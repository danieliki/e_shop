﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using E_Shop.Application;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;
using E_Shop.Web.Areas.Admin.Models.Variety;
using E_Shop.Web.JQueryDataTable;
using RestSharp;

namespace E_Shop.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Controlador de las variedades - Solo accesible desde el rol Admin
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class VarietyController : Controller
    {
        IProductManager productManager;
        IVarietyManager varietyManager;

        /// <summary>
        /// Constructor del controlador
        /// </summary>
        /// <param name="productManager"></param>
        /// <param name="varietyManager"></param>
        public VarietyController(IProductManager productManager,  VarietyManager varietyManager)
        {
            this.productManager = productManager;
            this.varietyManager = varietyManager;
        }

        /// <summary>
        /// Método que devuelve todas las variedades en formato JSon para pintar una tabla con Ajax
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexData()
        {
            JQueryDataTableRequest request = new JQueryDataTableRequest(System.Web.HttpContext.Current.Request);
            var model = varietyManager.GetAll()
                .Select(e => new VarietyIndexViewModel()
                {
                    Id = e.Id,
                    Product = e.Product.Brand + "-" + e.Product.Model,
                    Stock = e.Stock,
                    OnSale = e.OnSale,
                    DateFrom = e.DateFrom,
                    Colour = e.Colour,
                    Weight = e.Weight,
                    Material = e.Material,
                });
            JQueryDataTableResponse result = JQueryHelper.CreateJQueryDataTableResponse(model, request);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que muestra una tabla con todas las variedades
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var model =
                varietyManager.GetAll()
                    .Select(e => new VarietyIndexViewModel()
                            {
                        Id = e.Id,
                        Product = e.Product.Brand + "-" + e.Product.Model,
                        Stock = e.Stock,
                        OnSale = e.OnSale,
                        DateFrom = e.DateFrom,
                        Colour = e.Colour,
                        Weight = e.Weight,
                        Material = e.Material,
                    });

            return View(model);
        }

        /// <summary>
        /// Método que pinta la vista de creación de una variedad
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            VarietyCreateViewModel model = new VarietyCreateViewModel();
            model = PrepareViewModel(model);
            return View(model);
        }

        /// <summary>
        /// Método que crea una variedad
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(VarietyCreateViewModel model)
        {
            var id = model.Variety.ProductId;
            model.Product = productManager.GetById(id);                        
            try
            {
                if (ModelState.IsValid)
                {
                    model.Variety.Permalink = string.Join("_", model.Product.Brand + model.Product.Model + model.Variety.Id).ToLower();
                    model.Variety.DateFrom = DateTime.Today;
                    varietyManager.Add(model.Variety);
                    varietyManager.db.SaveChanges();

                    return RedirectToAction("Index");
                }
                model = PrepareViewModel(model);
                return View(model);
            }
            catch (DbEntityValidationException e)
            {
                return View("Error" + e);
            }
        }

        /// <summary>
        /// Método que pinta la vista de edición de una variedad
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            VarietyCreateViewModel model = new VarietyCreateViewModel();
            model = PrepareViewModel(model);
            model.Variety = varietyManager.GetById(id);
            model.ImagesFiles = new List<string>();

            var referenceUri = new Uri(@Server.MapPath("~\\Images\\Product\\"));

            foreach (var file in Directory.GetFiles(@Server.MapPath("~\\Images\\Product\\"), id + "_*"))
            {
                var fileUri = new Uri(file);
                model.ImagesFiles.Add("/Images/Product/" + referenceUri.MakeRelativeUri(fileUri));
            }

            return View(model);
        }

        /// <summary>
        /// Método de edición de una variedad
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(int id, VarietyCreateViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.Images != null)
                    {
                        foreach (var image in model.Images)
                        {
                            image.SaveAs(Path.Combine(@Server.MapPath("~\\Images\\Product\\"), id + "_" + model.Images.IndexOf(image) + ".jpg"));
                        }
                    }
                    varietyManager.Edit(id, model.Variety);
                    varietyManager.db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch
            {
                model = PrepareViewModel(model);
                return View(model);
            }
        }

        /// <summary>
        /// Método que prepara los dropdownlist de la vista
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private VarietyCreateViewModel PrepareViewModel(VarietyCreateViewModel model)
        {
            model.Products = productManager.GetAll().Select(f => new SelectListItem()
            {
                Value = f.Id.ToString(),
                Text = f.Brand + "-" + f.Model
            }).ToList();
            model.Products.Insert(0, new SelectListItem() { Value = "0", Text = "Seleccione una opción", Selected = true });
            return model;
        }

        /// <summary>
        /// Método que devuelve los resultados del web service
        /// </summary>
        /// <returns></returns>
        public ActionResult TestService()
        {
            var client = new RestClient("http://localhost:65371/");
            var request = new RestRequest("api/Variety", Method.GET) { RequestFormat = DataFormat.Json };
            var response = client.Execute<List<Variety>>(request);

            if (response.Data == null)
            {
                throw new Exception(response.ErrorMessage);
            }
            return View(response.Data);
        }
    }
}
