﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;
using E_Shop.Web.Areas.Admin.Models.IVA;
using E_Shop.Web.JQueryDataTable;
using RestSharp;

namespace E_Shop.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Controlador de los IVAs - Solo accesible con el rol Admin
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class IVAController : Controller
    {
        private IIVAManager ivaManager;

        /// <summary>
        /// Constructor del controlador
        /// </summary>
        /// <param name="ivaManager"></param>
        public IVAController(IIVAManager ivaManager)
        {
            this.ivaManager = ivaManager;
        }

        /// <summary>
        /// Método que devuelve todos los IVAs en formato JSon para pintar una tabla con Ajax
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexData()
        {
            JQueryDataTableRequest request = new JQueryDataTableRequest(System.Web.HttpContext.Current.Request);
            var model =
                ivaManager.GetAll()
                    .Select(e => new IVAIndexViewModel()
                    {
                        Id = e.Id,
                        Name = e.Name,
                        Value = e.Value
                    });

            JQueryDataTableResponse result = JQueryHelper.CreateJQueryDataTableResponse(model, request);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que muestra una tabla con todos los IVAs
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var model =
               ivaManager.GetAll()
                   .Select(e => new IVAIndexViewModel()
                   {
                       Id = e.Id,
                       Name = e.Name,
                       Value = e.Value
                   });

            return View(model);
        }

        /// <summary>
        /// Método que pinta la vista de creación de un IVA
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            IVACreateViewModel model = new IVACreateViewModel();

            return View(model);
        }

        /// <summary>
        /// Método que crea un IVA
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(IVACreateViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ivaManager.Add(model.IVA);
                    ivaManager.db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch (DbEntityValidationException e)
            {
                return View("Error" + e);
            }
        }

        /// <summary>
        /// Método que pinta la vista de edición de un IVA
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            IVACreateViewModel model = new IVACreateViewModel();
            model.IVA = ivaManager.GetById(id);

            return View(model);
        }

        /// <summary>
        /// Método de edición de un IVA
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(int id, IVACreateViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ivaManager.Edit(id, model.IVA);
                    ivaManager.db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch (Exception ex)
            {
                return View(model);
            }
        }

        /// <summary>
        /// Método que devuelve los resultados del web service
        /// </summary>
        /// <returns></returns>
        public ActionResult TestService()
        {
            var client = new RestClient("http://localhost:65371/");
            var request = new RestRequest("api/IVA", Method.GET) { RequestFormat = DataFormat.Json };
            var response = client.Execute<List<IVA>>(request);

            if (response.Data == null)
            {
                throw new Exception(response.ErrorMessage);
            }
            return View(response.Data);
        }
    }
}
