﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;
using E_Shop.Web.Areas.Admin.Models.Product;
using E_Shop.Web.JQueryDataTable;
using RestSharp;

namespace E_Shop.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Controlador de los productos - Solo accesible con el rol Admin
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class ProductController : Controller
    {
        private IProductManager productManager;
        private IFamilyManager familyManager;
        private IIVAManager ivaManager;

        /// <summary>
        /// Constructor del controlador
        /// </summary>
        /// <param name="productManager"></param>
        /// <param name="familyManager"></param>
        /// <param name="ivaManager"></param>
        public ProductController(IProductManager productManager, IFamilyManager familyManager, IIVAManager ivaManager)
        {
            this.productManager = productManager;
            this.familyManager = familyManager;
            this.ivaManager = ivaManager;
        }

        /// <summary>
        /// Método que devuelve todos los productos en formato JSon para pintar una tabla con Ajax
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexData()
        {
            JQueryDataTableRequest request = new JQueryDataTableRequest(System.Web.HttpContext.Current.Request);
            var model =
                productManager.GetAll("Family")
                    .Select(e => new ProductIndexViewModel()
                            {
                                Id = e.Id,
                                Brand = e.Brand,
                                Model = e.Model,
                                Price = e.Price,
                                IVA = e.IVA.Value,
                                Family = e.Family.Name
                            });

            JQueryDataTableResponse result = JQueryHelper.CreateJQueryDataTableResponse(model, request);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que muestra una tabla con todos los productos
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var model =
                productManager.GetAll("Family")
                    .Select(e => new ProductIndexViewModel()
                    {
                        Id = e.Id,
                        Brand = e.Brand,
                        Model = e.Model,
                        Price = e.Price,
                        IVA = e.IVA.Value,
                        Family = e.Family.Name
                    });

            return View(model);
        }

        /// <summary>
        /// Método que pinta la vista de creación de un producto
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            ProductCreateViewModel model = new ProductCreateViewModel();
            model = PrepareViewModel(model);

            return View(model);
        }

        /// <summary>
        /// Método que crea un producto
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(ProductCreateViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                { 
                    productManager.Add(model.Product);
                    productManager.db.SaveChanges();
                    return RedirectToAction("Index");
                }
                model = PrepareViewModel(model);
                return View(model);
            }
            catch (DbEntityValidationException e)
            {
                return View("Error" + e);
            }
        }

        /// <summary>
        /// Método que pinta la vista de edición de un producto
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            ProductCreateViewModel model = new ProductCreateViewModel();
            model = PrepareViewModel(model);
            model.Product = productManager.GetById(id);

            return View(model);
        }

        /// <summary>
        /// Método de edición de un producto
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(int id, ProductCreateViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    productManager.Edit(id, model.Product);
                    productManager.db.SaveChanges();
                    return RedirectToAction("Index");
                }
                model = PrepareViewModel(model);
                return View(model);
            }
            catch(Exception)
            {
                return View(model);
            }
        }

        /// <summary>
        /// Método que prepara los dropdownlist de la vista
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private ProductCreateViewModel PrepareViewModel(ProductCreateViewModel model)
        {
            model.Families =
                familyManager.GetAll()
                    .Select(f => new SelectListItem() { Value = f.Id.ToString(), Text = f.Name })
                    .ToList();
            model.Families.Insert(
                0,
                new SelectListItem() { Value = "0", Text = "Seleccione una opción", Selected = true });
            model.IVAS =
                ivaManager.GetAll()
                    .Select(i => new SelectListItem() { Value = i.Id.ToString(), Text = i.Name })
                    .ToList();
            model.IVAS.Insert(
                0,
                new SelectListItem() { Value = "0", Text = "Seleccione una opción", Selected = true });
            return model;
        }

        /// <summary>
        /// Método que devuelve los resultados del web service
        /// </summary>
        /// <returns></returns>
        public ActionResult TestService()
        {
            var client = new RestClient("http://localhost:65371/");
            var request = new RestRequest("api/Product", Method.GET) { RequestFormat = DataFormat.Json };
            var response = client.Execute<List<Product>>(request);

            if (response.Data == null)
            {
                throw new Exception(response.ErrorMessage);
            }
            return View(response.Data);
        }
    }
}