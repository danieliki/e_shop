﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;
using E_Shop.Web.Areas.Admin.Models.Comment;
using E_Shop.Web.JQueryDataTable;
using RestSharp;

namespace E_Shop.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Controlador de los comentarios - Solo accesible con el rol Admin
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class CommentController : Controller
    {
        private ICommentManager commentManager;
        private IVarietyManager varietyManager;

        /// <summary>
        /// Constructor del controlador
        /// </summary>
        /// <param name="commentManager"></param>
        public CommentController(ICommentManager commentManager, IVarietyManager varietyManager)
        {
            this.commentManager = commentManager;
            this.varietyManager = varietyManager;
        }

        /// <summary>
        /// Método que devuelve todos los comentarios en formato JSon para pintar una tabla con Ajax
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexData()
        {
            JQueryDataTableRequest request = new JQueryDataTableRequest(System.Web.HttpContext.Current.Request);
            var model =
                commentManager.GetAll()
                    .Select(e => new CommentIndexViewModel()
                    {
                        Id = e.Id,
                        Variety = e.Variety.Product.Brand + e.Variety.Product.Model,
                        User = e.User,
                        Date = e.DateTime,
                        Text = e.Text
                    });

            JQueryDataTableResponse result = JQueryHelper.CreateJQueryDataTableResponse(model, request);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Método que muestra una tabla con todos los comentarios
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var model =
                commentManager.GetAll()
                    .Select(e => new CommentIndexViewModel()
                    {
                        Id = e.Id,
                        Variety = e.Variety.Product.Brand + e.Variety.Product.Model,
                        User = e.User,
                        Date = e.DateTime,
                        Text = e.Text
                    });


            return View(model);
        }

        /// <summary>
        /// Método que pinta la vista de edición de un comentario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            CommentCreateViewModel model = new CommentCreateViewModel();
            model = PrepareViewModel(model);
            model.Comment = commentManager.GetById(id);

            return View(model);
        }

        /// <summary>
        /// Método de edición de un comentario
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(int id, CommentCreateViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    commentManager.Edit(id, model.Comment);
                    commentManager.db.SaveChanges();
                    return RedirectToAction("Index");
                }
                model = PrepareViewModel(model);
                return View(model);
            }
            catch (Exception)
            {
                return View(model);
            }
        }

        /// <summary>
        /// Método que prepara los dropdownlist de la vista
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private CommentCreateViewModel PrepareViewModel(CommentCreateViewModel model)
        {
            model.Variety =
                varietyManager.GetAll()
                    .Select(f => new SelectListItem() { Value = f.Id.ToString(), Text = f.Product.Brand + f.Product.Model })
                    .ToList();
            return model;
        }

        /// <summary>
        /// Método que devuelve los resultados del web service
        /// </summary>
        /// <returns></returns>
        public ActionResult TestService()
        {
            var client = new RestClient("http://localhost:65371/");
            var request = new RestRequest("api/Comment", Method.GET) { RequestFormat = DataFormat.Json };
            var response = client.Execute<List<Comment>>(request);

            if (response.Data == null)
            {
                throw new Exception(response.ErrorMessage);
            }
            return View(response.Data);
        }
    }
}
