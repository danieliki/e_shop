﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using E_Shop.Web.App_GlobalResources;

namespace E_Shop.Web.Areas.Admin.Models.Product
{
    public class ProductCreateViewModel
    {
        [Display(ResourceType = typeof(Language), Name = "PRODUCT")]
        public CORE.Domain.Classes.Product Product { get; set; }

        [Display(ResourceType = typeof(Language), Name = "FAMILY")]
        public List<SelectListItem> Families { get; set; }

        public List<SelectListItem> IVAS { get; set; }
    }
}