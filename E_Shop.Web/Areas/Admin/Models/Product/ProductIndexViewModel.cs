﻿using System.ComponentModel.DataAnnotations;
using E_Shop.Web.App_GlobalResources;

namespace E_Shop.Web.Areas.Admin.Models.Product
{

    /// <summary>
    /// ViewModel para mostrar el listado del producto de administración
    /// </summary>
    public class ProductIndexViewModel
    {
        public int Id { get; set; }

        public string Brand { get; set; }

        public string Model { get; set; }

        [Display(ResourceType = typeof(Language), Name = "PRICE")]
        [DisplayFormat(DataFormatString = "{0:0,00}", ApplyFormatInEditMode = true)]
        public decimal Price { get; set; }

        public decimal IVA { get; set; }

        [Display(ResourceType = typeof(Language), Name = "FAMILY")]
        public string Family { get; set; }
    }
}