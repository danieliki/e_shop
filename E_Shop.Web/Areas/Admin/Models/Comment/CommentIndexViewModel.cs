﻿using System;

namespace E_Shop.Web.Areas.Admin.Models.Comment
{
    public class CommentIndexViewModel
    {
        public int Id { get; set; }

        public string Variety { get; set; }

        public string User { get; set; }

        public DateTime Date { get; set; }

        public string Text { get; set; }
    }
}