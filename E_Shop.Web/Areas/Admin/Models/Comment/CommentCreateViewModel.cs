﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace E_Shop.Web.Areas.Admin.Models.Comment
{
    public class CommentCreateViewModel
    {
        public CORE.Domain.Classes.Comment Comment { get; set; }

        public List<SelectListItem> Variety { get; set; }
    }
}