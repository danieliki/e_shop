﻿using System;

namespace E_Shop.Web.Areas.Admin.Models.Order
{
    public class OrderIndexViewModel
    {
        public int Id { get; set; }

        public string User { get; set; }

        public DateTime Date { get; set; }

        public string PaymentMethod { get; set; }

        public bool IsPaid { get; set; }

        public string Status { get; set; }

        public string TrakingNumber { get; set; }
    }
}