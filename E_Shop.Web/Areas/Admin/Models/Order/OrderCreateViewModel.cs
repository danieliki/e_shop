﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace E_Shop.Web.Areas.Admin.Models.Order
{
    public class OrderCreateViewModel
    {
        public CORE.Domain.Classes.Order Order { get; set; }

        public IEnumerable<SelectListItem> Status { get; set; }
    }
}