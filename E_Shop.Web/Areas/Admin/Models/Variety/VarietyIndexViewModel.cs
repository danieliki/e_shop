﻿using System;

namespace E_Shop.Web.Areas.Admin.Models.Variety
{

    /// <summary>
    /// ViewModel para mostrar el listado del variedades de producto de administración
    /// </summary>
    public class VarietyIndexViewModel
    {
        public int Id { get; set; }
       
        public string Product { get; set; }

        public int Stock { get; set; }

        public bool OnSale { get; set; }

        public DateTime? DateFrom { get; set; }

        public string Colour { get; set; }

        public decimal Weight { get; set; }

        public string Material { get; set; }
    }
}