﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using E_Shop.Web.App_GlobalResources;

namespace E_Shop.Web.Areas.Admin.Models.Variety
{
    public class VarietyCreateViewModel
    {
        [Display(ResourceType = typeof(Language), Name = "VARIETY")]
        public CORE.Domain.Classes.Variety Variety { get; set; }

        public CORE.Domain.Classes.Product Product { get; set; }

        [Display(ResourceType = typeof(Language), Name = "PRODUCT")]
        public List<SelectListItem> Products { get; set; }


        public List<HttpPostedFileBase> Images { get; set; }
        public List<string> ImagesFiles { get; set; }
    }
}