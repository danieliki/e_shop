﻿namespace E_Shop.Web.Areas.Admin.Models.IVA
{
    public class IVAIndexViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal Value { get; set; }
    }
}