﻿namespace E_Shop.Web.Areas.Admin.Models.IVA
{
    public class IVACreateViewModel
    {
        public CORE.Domain.Classes.IVA IVA { get; set; }
    }
}