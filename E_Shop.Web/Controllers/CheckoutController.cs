﻿using System;
using System.Linq;
using System.Web.Mvc;
using E_Shop.Application;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;
using E_Shop.CORE.Domain.Enums;
using E_Shop.Web.Models.Order;

namespace E_Shop.Web.Controllers
{
    /// <summary>
    /// Controlador de la confirmación de la compra
    /// </summary>
    [Authorize]
    public class CheckoutController : Controller
    {
        private IShoppingCartManager shoppingCartManager;
        private IOrderManager orderManager;
        private IOrderDetailManager orderDetailManager;

        /// <summary>
        /// Constructor del controlador 
        /// </summary>
        /// <param name="shoppingCartManager"></param>
        /// <param name="orderManager"></param>
        /// <param name="orderDetailManager"></param>
        public CheckoutController(IShoppingCartManager shoppingCartManager, IOrderManager orderManager, IOrderDetailManager orderDetailManager)
        {
            this.shoppingCartManager = shoppingCartManager;
            this.orderManager = orderManager;
            this.orderDetailManager = orderDetailManager;
        }

        /// <summary>
        /// Método que crea la vista de datos del pedido
        /// </summary>
        /// <returns></returns>
        public ActionResult AddressAndPayment()
        {
            OrderViewModel model = new OrderViewModel();
            return View(model);
        }

        /// <summary>
        /// Método que guarda los datos del pedido
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddressAndPayment(Order order)
        {
            var cart = Cart.GetCart(HttpContext);
            TryUpdateModel(order);
            try
            {
                ModelState["Order.BillingAddress.PostalCode"].Errors.Clear();
                ModelState["Order.ShippingAddress.PostalCode"].Errors.Clear();
                if (ModelState.IsValid)
                {
                    order.User = User.Identity.Name;
                    order.OrderDate = DateTime.Now;
                    order.TrackingNumber = Guid.NewGuid();
                    order.Status = Status.Reserved;

                    if (order.PaymentMethod.Name == "transfer")
                    {
                        order.IsPaid = false;
                    }
                    else
                    {
                        order.IsPaid = true;
                    }
                    
                    orderManager.Add(order);
                    orderManager.db.SaveChanges();
                }
                
                cart.CreateOrder(order);

                order.OrderDetails = orderDetailManager.GetAllByOrderId(order.Id).ToList();

                orderManager.Edit(order.Id, order);
                orderManager.db.SaveChanges();

                return RedirectToAction("Complete", new { id = order.Id });
            }
            catch (Exception ex)
            {
                throw new Exception("Ha ocurrido un problema al crear el pedido" + ex);
            }
        }

        /// <summary>
        /// Método que migra un carrito a otro en función de la sesión
        /// </summary>
        /// <param name="userName"></param>
        public void MigrateCart(string userName)
        {
            var cart = Cart.GetCart(HttpContext);
            var shoppingCart = shoppingCartManager.GetAll().Where(c => c.CartId == cart.ShoppingCartId);

            foreach (ShoppingCart item in shoppingCart)
            {
                item.CartId = userName;
            }
            shoppingCartManager.db.SaveChanges();
        }

        /// <summary>
        /// Método que confirma un pedido una vez realizado 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Complete(int id)
        {
            bool isValid = orderManager.GetAll().Any(o => o.Id == id && o.User == User.Identity.Name);
            var order = orderManager.GetById(id);
            var orderDetails = orderDetailManager.GetAllByOrderId(order.Id).ToList();

            if (isValid)
            {
                OrderViewModel model = new OrderViewModel()
                {
                    Username = order.User,
                    Order = order,
                    OrderDetails = orderDetails
                };

                if (order.BillingAddress.PostalCode == 0)
                {
                    order.BillingAddress = order.ShippingAddress;
                }

                return View(model);
            }
            return View("Error");
        }
    }
}
