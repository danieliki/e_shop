﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;
using E_Shop.Web.Models.Shop;

namespace E_Shop.Web.Controllers
{
    
    /// <summary>
    /// Controlador de la parte de muestra de variedades de la tienda
    /// </summary>
    public class ShopController : Controller
    {
        private IFamilyManager familyManager;
        private IVarietyManager varietyManager;
        private ICommentManager commentManager;

        /// <summary>
        /// Constructor del controlador
        /// </summary>
        /// <param name="familyManager"></param>
        /// <param name="varietyManager"></param>
        /// <param name="commentManager"></param>
        public ShopController(IFamilyManager familyManager, IVarietyManager varietyManager, ICommentManager commentManager)
        {
            this.familyManager = familyManager;
            this.varietyManager = varietyManager;
            this.commentManager = commentManager;
        }

       

        /// <summary>
        /// Método para mostrar un listado de variedades (las 12 primeras)
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var model = varietyManager.GetAll()
                    .Select(p => new ListViewModel()
                    {
                        Id = p.Id,
                        Name = p.Product.Brand + " - " + p.Product.Model,
                        Brand = p.Product.Brand,
                        Price = p.Product.Price
                    }).ToList();

            return View(model);
        }

        /// <summary>
        /// Método para filtrar las variedades por familia
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ListByFamily(int id)  
        {
            var model = varietyManager.GetAllByProductFamily(id)
                    .Select(p => new ListViewModel()
                    {
                        Id = p.Id,
                        Name = p.Product.Brand + " - " + p.Product.Model,
                        Price = p.Product.Price,
                    }).ToList();

            return PartialView("_ListPartial", model);
        }

        /// <summary>
        /// Método de llena las propiedades de la clase FamilyViewModel, deuna manera recursiva
        /// </summary>
        /// <param name="families"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        private List<FamilyMenuViewModel> GetElements(List<Family> families, int? parentId = null)
        {
            List<FamilyMenuViewModel> result = new List<FamilyMenuViewModel>();
            foreach (var family in families.Where(f => f.ParentId == parentId))
            {
                result.Add(new FamilyMenuViewModel()
                {
                    ParentFamily = family,
                    SonFamily = GetElements(families, family.Id)
                });
            }
            return result;
        }

        /// <summary>
        /// Método que pinta todas las familias y subfamilias de una variedad en una vista parcial para fitrar por ellas
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult FamilyMenu()
        {
            var families = familyManager.GetAll().ToList();
            FamilyMenuViewModel model = GetElements(families).FirstOrDefault();

            return PartialView("_FamilyMenu", model);
        }

        /// <summary>
        /// Método para filtrar las variedades por marca
        /// </summary>
        /// <param name="brand"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ListByBrand(string brand)
        {
            var model = varietyManager.GetAllByProductBrand(brand)
                    .Select(p => new ListViewModel()
                    {
                        Id = p.Id,
                        Name = p.Product.Brand + " - " + p.Product.Model,
                        Price = p.Product.Price
                    }).ToList();

            return PartialView("_ListPartial", model);
        }

        /// <summary>
        /// Método para filtrar las variedades por precio
        /// </summary>
        /// <param name="price1"></param>
        /// <param name="price2"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ListByPrice(decimal price1, decimal price2)
        {
            var model = varietyManager.GetAllByProductPriceRange(price1, price2)
                    .Select(p => new ListViewModel()
                    {
                        Id = p.Id,
                        Name = p.Product.Brand + " - " + p.Product.Model,
                        Price = p.Product.Price,
                    }).ToList();

            return PartialView("_ListPartial", model);
        }

        /// <summary>
        /// Método que muestra los detalles de una variedad
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Details(int id)
        {
            var variety = varietyManager.GetById(id);
            var comments = commentManager.GetAllByVarietyId(id).ToList();
            var referenceUri = new Uri(@Server.MapPath("~\\Images\\Product\\"));
            var imageFiles = Directory.GetFiles(@Server.MapPath("~\\Images\\Product\\"), id + "_*")
                .Select(file => new Uri(file)).Select(fileUri => "/Images/Product/" + referenceUri.MakeRelativeUri(fileUri).ToString()).ToList();

            var quantity = new List<SelectListItem>();
            for (var i = 1; i < variety.Stock + 1; i++)
                quantity.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });

            ViewBag.list = quantity;

            DetailsViewModel model = new DetailsViewModel()
                                         {
                                             Id = id,
                                             Name = variety.Product.FullName,
                                             Price = variety.Product.Price.ToString("n2"),
                                             Weight = variety.Weight.ToString("n2"),
                                             Variety = variety,
                                             ImagesFiles = imageFiles,
                                             Image = imageFiles.FirstOrDefault(),
                                             Comments = comments,
                                             Stock = variety.Stock,
                                             Quantity = quantity
            };

            return View(model);
        }
        

        /// <summary>
        /// Método que pinta la pantalla de creción de un comentario de una variedad
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult CreateComment(int id)
        {
            DetailsViewModel model = new DetailsViewModel();
            model.Id = id;

            return View(model);
        }

        /// <summary>
        /// Método de creación de un comentario de una variedad
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateComment(DetailsViewModel model, int id)
        {
            model.User = User.Identity.Name;
            model.Date = DateTime.Now;
            model.Id = id;

            try
            {
                if (ModelState.IsValid)
                {
                     var comment = new Comment()
                                          {
                                              Id = new int(),
                                              User = model.User,
                                              DateTime = model.Date,
                                              VarietyId = model.Id,
                                              Text = model.Text
                                          };

                    commentManager.Add(comment);
                    commentManager.db.SaveChanges();
                    return RedirectToAction("Details", new { id = model.Id });
                }
                return View(model);
            }
            catch (Exception e)
            {
                return View("Error" + e);
            }
        }
    }
}
