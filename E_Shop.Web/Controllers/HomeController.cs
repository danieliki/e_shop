﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;
using E_Shop.Web.Models.Shop;

namespace E_Shop.Web.Controllers
{
    public class HomeController : Controller
    {   
        private IFamilyManager familyManager;

        public HomeController(IFamilyManager familyManager)
        {
            this.familyManager = familyManager;
        }

        /// <summary>
        /// Método que pinta todas las familias y subfamilias de una variedad en una vista parcial para crear un menú
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Menu()
        {
            var families = familyManager.GetAll().ToList();
            FamilyMenuViewModel model = GetElements(families).FirstOrDefault();

            return PartialView("_Menu", model);
        }

        /// <summary>
        /// Método de llena las propiedades de la clase FamilyViewModel, deuna manera recursiva
        /// </summary>
        /// <param name="families"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        private List<FamilyMenuViewModel> GetElements(List<Family> families, int? parentId = null)
        {
            List<FamilyMenuViewModel> result = new List<FamilyMenuViewModel>();
            foreach (var family in families.Where(f => f.ParentId == parentId))
            {
                result.Add(new FamilyMenuViewModel()
                {
                    ParentFamily = family,
                    SonFamily = GetElements(families, family.Id)
                });
            }
            return result;
        }

       
    }
}