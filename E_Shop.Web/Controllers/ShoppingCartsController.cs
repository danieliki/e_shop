﻿using System;
using System.Linq;
using System.Web.Mvc;
using E_Shop.Application;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;
using E_Shop.Web.Models.ShoppingCart;

namespace E_Shop.Web.Controllers
{
    /// <summary>
    /// Controlador del proceso de llenado del carrito de la compra
    /// </summary>
    public class ShoppingCartsController : Controller
    {
        private IVarietyManager varietyManager;
        private IShoppingCartManager shoppingCartManager;

        /// <summary>
        /// Constructor del controlador
        /// </summary>
        /// <param name="varietyManager"></param>
        /// <param name="shoppingCartManager"></param>
        public ShoppingCartsController(IVarietyManager varietyManager, IShoppingCartManager shoppingCartManager)
        {
            this.varietyManager = varietyManager;
            this.shoppingCartManager = shoppingCartManager;
        }

        /// <summary>
        /// Método que muestra el carrito con todos los items por sesión
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var cart = Cart.GetCart(HttpContext);
            decimal total = cart.GetTotal();
            decimal iva = cart.GetIVA() * total;
            if (ModelState.IsValid)
            {
                var viewModel = new ShoppingCartViewModel
                                    {
                                        CartItems = cart.GetCartItems(),
                                        CartTotal = decimal.Round(total, 2, MidpointRounding.AwayFromZero),
                                        Subtotal = total - decimal.Round(iva, 2, MidpointRounding.AwayFromZero),
                                        IVA = decimal.Round(iva, 2, MidpointRounding.AwayFromZero)
                };
                return View(viewModel);
            }
            return View();
        }

        /// <summary>
        /// Método que añade una variedad / item al carrito
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cantidad"></param>
        /// <returns></returns>
        public ActionResult AddToCart(int id, int cantidad)
        {
            var cart = Cart.GetCart(HttpContext);
            var variety = varietyManager.GetById(id);
            var cartItem =
                shoppingCartManager.GetAll()
                    .SingleOrDefault(c => c.CartId == cart.ShoppingCartId && c.Variety.Id == variety.Id);

            variety.Stock = variety.Stock - cantidad;
            varietyManager.Edit(id, variety);
            varietyManager.db.SaveChanges();

            if (cartItem == null)
            {
                cartItem = new ShoppingCart
                {
                    VarietyId = variety.Id,
                    CartId = cart.ShoppingCartId,
                    Count = cantidad,
                    DateCreated = DateTime.Now,
                };
                shoppingCartManager.Add(cartItem);
            }
            else
            {
                cartItem.Count++;
            }
            shoppingCartManager.db.SaveChanges();

            Session["varietyId"] = id;

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Método que elimina por llamada Ajax un item del carrito
        /// </summary>
        /// <param name="recordId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RemoveFromCart(int recordId)
        {
            var cart = Cart.GetCart(HttpContext);
            var cartItem = shoppingCartManager.GetAll().Single(c => c.CartId == cart.ShoppingCartId && c.RecordId == recordId);
            var varietyId = (int)Session["varietyId"];
            var variety = varietyManager.GetById(varietyId);

            int itemCount = 0;

            if (cartItem != null)
            {                
                if (cartItem.Count > 1)
                {
                    cartItem.Count--;
                    itemCount = cartItem.Count;
                }
                else
                {
                    shoppingCartManager.Delete(cartItem); 
                }

                variety.Stock = variety.Stock + 1;
                varietyManager.Edit(varietyId, variety);
                varietyManager.db.SaveChanges();

                shoppingCartManager.db.SaveChanges();
            }

            var results = new ShoppingCartRemoveViewModel
                              {
                                  CartTotal = cart.GetTotal(),
                                  CartCount = cart.GetCount(),
                                  ItemCount = itemCount,
                                  DeleteId = recordId
            };
            return Json(results);
        }

        /// <summary>
        /// Método que muestra en el menú el número de elementos que contiene el carrito
        /// </summary>
        /// <returns></returns>
        [ChildActionOnly]
        public ActionResult CartSummary()
        {
            var cart = Cart.GetCart(HttpContext);

            ViewData["CartCount"] = cart.GetCount();
            return PartialView("_CartSummary");
        }
    }
}