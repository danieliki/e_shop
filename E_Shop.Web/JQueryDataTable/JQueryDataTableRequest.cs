﻿using System.Collections;
using System.Collections.Generic;
using System.Web;

namespace E_Shop.Web.JQueryDataTable
{
    /// <summary>
    /// Class that encapsulates most common parameters sent by DataTables plugin
    /// </summary>
    public class JQueryDataTableRequest
    {
        /// <summary>
        /// Request sequence number sent by DataTable,
        /// same value must be returned in response
        /// </summary>       
        public int sEcho { get; set; }

        /// <summary>
        /// Number of columns in table
        /// </summary>
        public int iColumns { get; set; }

        /// <summary>
        /// Comma separated list of column names
        /// </summary>
        public string sColumns { get; set; }

        /// <summary>
        /// First record that should be shown(used for paging)
        /// </summary>
        public int iDisplayStart { get; set; }

        /// <summary>
        /// Number of records that should be shown in table
        /// </summary>
        public int iDisplayLength { get; set; }

        public List<string> mDataProp { get; set; }
        public List<string> Search { get; set; }
        public List<string> bRegex { get; set; }
        public List<string> bSearchable { get; set; }
        public List<string> bSortable { get; set; }
        public string sSearch { get; set; }


        /// <summary>
        /// Text used for filtering
        /// </summary>

        public List<string> iSortCol { get; set; }
        public List<string> sSortDir { get; set; }


        /// <summary>
        /// Number of columns that are used in sorting
        /// </summary>
        public int iSortingCols { get; set; }



        public JQueryDataTableRequest(HttpRequest request)
        {
            foreach (var param in request.Params)
            {
                if (param != null)
                {
                    string name = param.ToString();
                    int pos = name.IndexOf('_');
                    var property = this.GetType().GetProperty(name.Substring(0, pos > 0 ? pos : name.Length));
                    if (property != null)
                    {
                        if (property.PropertyType == typeof(List<string>))
                        {
                            if (property.GetValue(this) == null)
                            {
                                property.SetValue(this, new List<string>());
                            }
                            ((IList)property.GetValue(this)).Add(request.Params[name]);
                        }
                        else if (property.PropertyType == typeof(int))
                        {
                            property.SetValue(this, int.Parse(request.Params[name]));
                        }
                        else
                        {
                            property.SetValue(this, request.Params[name]);
                        }
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine(name);
                    }
                }
            }

            //sEcho = int.Parse(request.Params["sEcho"]);
            //iColumns = int.Parse(request.Params["iColumns"]);
            //sColumns = request.Params["sColumns"];
            //iDisplayStart = int.Parse(HttpContext.Current.Request.Params["iDisplayStart"]);
            //iDisplayLength = int.Parse(HttpContext.Current.Request.Params["iDisplayLength"]);
            //mDataProp_0 = HttpContext.Current.Request.Params["mDataProp_0"];
            //Search_0 = HttpContext.Current.Request.Params["mDataProp_0"];
            //bRegex_0 = HttpContext.Current.Request.Params["bRegex_0"];
            //bSearchable_0 = HttpContext.Current.Request.Params["bSearchable_0"];
            //bSortable_0 = HttpContext.Current.Request.Params["bSortable_0"];
            //mDataProp_1 = HttpContext.Current.Request.Params["mDataProp_1"];
            //sSearch_1 = HttpContext.Current.Request.Params["sSearch_1"];
            //bRegex_1 = HttpContext.Current.Request.Params["bRegex_1"];
            //bSearchable_1 = HttpContext.Current.Request.Params["bSearchable_1"];
            //bSortable_1 = HttpContext.Current.Request.Params["bSortable_1"];
            //mDataProp_2 = HttpContext.Current.Request.Params["mDataProp_2"];
            //sSearch_2 = HttpContext.Current.Request.Params["sSearch_2"];
            //bRegex_2 = HttpContext.Current.Request.Params["bRegex_2"];
            //bSearchable_2 = HttpContext.Current.Request.Params["bSearchable_2"];
            //bSortable_2 = HttpContext.Current.Request.Params["bSortable_2"];
            //mDataProp_3 = HttpContext.Current.Request.Params["mDataProp_3"];
            //sSearch_3 = HttpContext.Current.Request.Params["sSearch_3"];
            //bRegex_3 = HttpContext.Current.Request.Params["bRegex_3"];
            //bSearchable_3 = HttpContext.Current.Request.Params["bSearchable_3"];
            //bSortable_3 = HttpContext.Current.Request.Params["bSortable_3"];
            //sSearch = HttpContext.Current.Request.Params["sSearch"];
            //bRegex = HttpContext.Current.Request.Params["bRegex"];
            //iSortCol_0 = HttpContext.Current.Request.Params["iSortCol_0"];
            //sSortDir_0 = HttpContext.Current.Request.Params["sSortDir_0"];
            //iSortingCols = int.Parse(HttpContext.Current.Request.Params["iSortingCols"]);
        }
    }
}
