﻿using System.Web.Optimization;

namespace E_Shop.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Scripts

            bundles.Add(
                new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-2.2.3.js"));

            bundles.Add(
                new ScriptBundle("~/bundles/jqueryval").Include(
                    "~/Scripts/jquery.validate*",
                    "~/Scripts/jQueryFixes.js",
                    "~/Scripts/jquery.validate.unobtrusive.js"));

            bundles.Add(
                new ScriptBundle("~/bundles/bootstrap").Include(
                    "~/Scripts/bootstrap.js", 
                    "~/Scripts/respond.js"));

            bundles.Add(
                new ScriptBundle("~/bundles/datatables").Include(
                "~/Scripts/DataTables/jquery.dataTables.js"));

            bundles.Add(
                new ScriptBundle("~/bundles/template").Include(
                "~/Scripts/main.js",
                "~/Scripts/jquery.scrollUp.min.js",
                "~/Scripts/jquery.prettyPhoto.js"));

            bundles.Add(
               new ScriptBundle("~/bundles/zoom").Include(
               "~/Scripts/elevatezoom.jquery.json",
               "~/Scripts/jquery.elevatezoom.js",
               "~/Scripts/jquery.elevateZoom-3.0.8.min.js"
               ));

            bundles.Add(
              new ScriptBundle("~/bundles/moment").Include(
              "~/Scripts/moment.min.js"));

            #endregion

            #region Modernizr

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));

            #endregion

            #region Content css

            bundles.Add(
                new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css", 
                "~/Content/site.css"));

            bundles.Add(
                new StyleBundle("~/Content/datatables").Include(
                "~/Content/DataTables/css/dataTables.bootstrap.css", 
                "~/Content/DataTables/css/jquery.dataTables.min.css"));

            bundles.Add(
                new StyleBundle("~/Content/template").Include(
                "~/Content/main.css",
                //"~/Content/bootstrap.min.css",
                "~/Content/font-awesome.min.css",
                "~/Content/prettyPhoto.css",
                "~/Content/responsive.css",
                "~/Content/animate.css"));

            #endregion
        }
    }
}
