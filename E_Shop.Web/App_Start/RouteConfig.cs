﻿using System.Web.Mvc;
using System.Web.Routing;

namespace E_Shop.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Permalink",
            //    url: "{controller}/{action}/{Permalink}",
            //    defaults: new { controller = "Shop", action = "Index", Permalink = "" }
            //);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Shop", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
