﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace E_Shop.Web.Models.ShoppingCart
{
    public class ShoppingCartViewModel
    {
        public List<CORE.Domain.Classes.ShoppingCart> CartItems { get; set; }

        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal CartTotal { get; set; }

        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal Subtotal { get; set; }

        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal IVA { get; set; }
    }
}