﻿namespace E_Shop.Web.Models.ShoppingCart
{
    public class ShoppingCartRemoveViewModel
    {
        public decimal CartTotal { get; set; }
        public int CartCount { get; set; }
        public int ItemCount { get; set; }
        public int DeleteId { get; set; }
    }
}