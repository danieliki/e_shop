﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

using E_Shop.CORE.Domain.Classes;

namespace E_Shop.Web.Models.Shop
{
    public class DetailsViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Price { get; set; }

        public string Weight { get; set; }

        public Variety Variety { get; set; }

        public string Image { get; set; }

        public List<string> ImagesFiles { get; set; }

        public List<Comment> Comments { get; set; }

        public string User { get; set; }

        public DateTime Date { get; set; }

        public Comment Comment { get; set; }

        public string Text { get; set; }

        public List<SelectListItem> Quantity{ get; set; }

        public int Stock { get; set; }
    }
}