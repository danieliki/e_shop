﻿using System.Collections.Generic;
using System.Linq;

using E_Shop.CORE.Domain.Classes;

namespace E_Shop.Web.Models.Shop
{
    public class FamilyMenuViewModel
    {
        public Family ParentFamily { get; set; }
        public List<FamilyMenuViewModel> SonFamily { get; set; }
    }
}