﻿using System.Collections.Generic;

using E_Shop.CORE.Domain.Classes;

namespace E_Shop.Web.Models.Shop
{
    public class ListViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Brand { get; set; }

        public decimal Price { get; set; }

        public string Image
        {
            get
            {
                return "/Images/Product/" + BuildNameImage(Id);
            }
            private set { }
        }

        private string BuildNameImage(int varietyId)
        {
            return $"{varietyId}_0.jpg";
        }
    }
}