﻿using System.Collections.Generic;
using E_Shop.CORE.Domain.Classes;

namespace E_Shop.Web.Models.Email
{
    public class EmailViewModel: Postal.Email
    {
        public int Id { get; set; }

        public string To { get; set; }

        public string Body { get; set; }

        public CORE.Domain.Classes.Order Order { get; set; }

        public List<OrderDetail> OrderDetails { get; set; }

        public string Image
        {
            get
            {
                return "/Images/Product/" + BuildNameImage(Id);
            }
            private set { }
        }

        private string BuildNameImage(int varietyId)
        {
            return $"{varietyId}_0.jpg";
        }
    }
}