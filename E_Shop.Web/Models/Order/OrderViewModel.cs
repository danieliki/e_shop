﻿using System.Collections.Generic;
using E_Shop.CORE.Domain.Classes;

namespace E_Shop.Web.Models.Order
{
    public class OrderViewModel
    {
        public ApplicationUser User { get; set; }

        public string Username { get; set; }

        public CORE.Domain.Classes.Order Order { get; set; }

        public List<OrderDetail> OrderDetails { get; set; }
    }
}