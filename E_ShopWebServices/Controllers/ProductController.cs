﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using E_Shop.Application;
using E_Shop.CORE.Contracts;
using E_Shop.CORE.Domain.Classes;
using E_Shop.DAL;

namespace E_ShopWebServices.Controllers
{
    public class ProductController : ApiController
    {
        public IEnumerable<Product> GetProducts()
        {
            //IProductManager productManager = IoC.Current.Resolve<IProductManager>();
            IApplicationDbContext db = new ApplicationDbContext();
            IProductManager productManager = new ProductManager(db);
            return productManager.GetAll().ToList();
        }

        public Product GetById(int id)
        {
            IApplicationDbContext db = new ApplicationDbContext();
            IProductManager productManager = new ProductManager(db);
            return productManager.GetById(id);
        }


    }
}
